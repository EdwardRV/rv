war_measures = {

	USA_homeland_defense = {

		icon = generic_prepare_civil_war

		allowed = {
			original_tag = USA
		}

		available = {
			any_state = {
				is_core_of = USA
				NOT = {
					is_controlled_by = USA
				}
			}
			has_civil_war = no

		}

		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 200
		}

		visible = {
			has_defensive_war = yes
			CSA = {
				exists = no
			}
		}

		complete_effect = {
			if = {
				limit = {
					has_war_support < 0.9
				}
				set_war_support = 0.9
			}
			if = {
				limit = {
					has_idea = great_depression
				}
				remove_ideas = great_depression
			}
			if = {
				limit = {
					NOT = {
						OR = {
							has_idea = extensive_conscription
							has_idea = scraping_the_barrel
							has_idea = service_by_requirement
							has_idea = all_adults_serve
						}
					}
				}
				add_ideas = extensive_conscription
			}
			if = {
				limit = {
					OR = {
						has_idea = undisturbed_isolation
						has_idea = isolation
						has_idea = civilian_economy
						has_idea = low_economic_mobilisation
					}
				}
				add_ideas = war_economy
			}
			add_ideas = USA_homeland_defense
		}
	}
	USA_CSA_set_up_provisional_government = { 
		available = {
			any_state = {
				is_core_of = USA
				is_fully_controlled_by = ROOT
			}
		}
		visible = {
			original_tag = CSA
			has_war_with = USA
		}
		cost = 0
		days_remove = 100
		fixed_random_seed = yes
		ai_will_do = {
			factor = 1
		}
		modifier = {
			political_power_gain = -0.25
		}
		remove_effect = {
			random_state = {
				limit = {
					not = { is_core_of = ROOT }
					is_core_of = USA
					is_fully_controlled_by = ROOT
				}
				remove_core_of = USA
				add_claim_by = USA
				add_core_of = CSA
				save_event_target_as = USA_state_government_created
			}
			USA = {
				country_event = mtg_usa_civil_war_fascists.203
			}
		}
	}
	USA_order_weapons_in_USB = {
	icon = generic_prepare_civil_war
		available = {
			custom_trigger_tooltip = {
				tooltip = USB_available_factories_tt
				check_variable = { USB.USB_factories_available > 5 }
			}
			any_neighbor_country = {
				tag = USB
			}
		}
		visible = {
			country_exists = USB
			OR = {
				original_tag = CSA
				original_tag = USA
			}
			not = { has_war_with = USB }
			USB = { is_in_faction = no }
		}
		days_remove = 45
		cost = 25
		ai_will_do = {
			factor = 10
			modifier = {
				has_equipment = {
					infantry_equipment < 1
				}
				factor = 50
			}
			modifier = {
				has_manpower < 10000 
				factor = 0 #need to up conscription laws
			}
		}
		modifier = {
			civilian_factory_use = 2
		}
		complete_effect = {
			add_to_variable = { USB.USB_factories_available = -3 }
			USB = {
				add_offsite_building = { type = industrial_complex level = 2 }
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = 2500
			}
			USB = {
				add_offsite_building = { type = industrial_complex level = -2 }
			}
			add_to_variable = { USB.USB_factories_available = 3 }
		}
	}
	USA_order_artillery_in_USB = {
		icon = ger_military_buildup
		available = {
			custom_trigger_tooltip = {
				tooltip = USB_available_factories_tt
				check_variable = { USB.USB_factories_available > 5 }
			}
			any_neighbor_country = {
				tag = USB
			}
		}
		visible = {
			country_exists = USB
			OR = {
				original_tag = CSA
				original_tag = USA
			}
			not = { has_war_with = USB }
			USB = { is_in_faction = no }
		}
		days_remove = 60
		cost = 50
		ai_will_do = {
			factor = 5
			modifier = {
				has_equipment = {
					artillery_equipment < 1
				}
				factor = 50
			}
			modifier = {
				has_manpower < 10000 
				factor = 0 #need to up conscription laws
			}
		}
		modifier = {
			civilian_factory_use = 5
		}
		complete_effect = {
			add_to_variable = { USB.USB_factories_available = -6 }
			USB = {
				add_offsite_building = { type = industrial_complex level = 5 }
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_equipment
				amount = 75
			}
			USB = {
				add_offsite_building = { type = industrial_complex level = -5 }
			}
			add_to_variable = { USB.USB_factories_available = 6 }
		}
	}
	USA_order_fighters_in_USB = {
		icon = generic_air
		available = {
			custom_trigger_tooltip = {
				tooltip = USB_available_factories_tt
				check_variable = { USB.USB_factories_available > 5 }
			}
			any_neighbor_country = {
				tag = USB
			}
		}
		visible = {
			country_exists = USB
			OR = {
				original_tag = CSA
				original_tag = USA
			}
			not = { has_war_with = USB }
			USB = { is_in_faction = no }
		}
		days_remove = 60
		cost = 50
		ai_will_do = {
			factor = 1
			modifier = {
				has_equipment = {
					fighter_equipment < 1
				}
				factor = 5
			}
			modifier = {
				has_manpower < 10000 
				factor = 0 #need to up conscription laws
			}
		}
		modifier = {
			civilian_factory_use = 5
		}
		complete_effect = {
			add_to_variable = { USB.USB_factories_available = -6 }
			USB = {
				add_offsite_building = { type = industrial_complex level = 5 }
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = fighter_equipment
				amount = 50
			}
			USB = {
				add_offsite_building = { type = industrial_complex level = -5 }
			}
			add_to_variable = { USB.USB_factories_available = 6 }
		}
	}
	USA_order_bombers_in_USB = {
		icon = generic_air
		available = {
			custom_trigger_tooltip = {
				tooltip = USB_available_factories_tt
				check_variable = { USB.USB_factories_available > 5 }
			}
			any_neighbor_country = {
				tag = USB
			}
		}
		visible = {
			country_exists = USB
			OR = {
				original_tag = CSA
				original_tag = USA
			}
			not = { has_war_with = USB }
			USB = { is_in_faction = no }
		}
		days_remove = 60
		cost = 50
		ai_will_do = {
			factor = 1
			modifier = {
				has_equipment = {
					tac_bomber_equipment < 1
				}
				factor = 5
			}
			modifier = {
				has_manpower < 10000 
				factor = 0 #need to up conscription laws
			}
		}
		modifier = {
			civilian_factory_use = 5
		}
		complete_effect = {
			add_to_variable = { USB.USB_factories_available = -6 }
			USB = {
				add_offsite_building = { type = industrial_complex level = 5 }
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = tac_bomber_equipment
				amount = 25
			}
			USB = {
				add_offsite_building = { type = industrial_complex level = -5 }
			}
			add_to_variable = { USB.USB_factories_available = 6 }
		}
	}
}
USA_aid_britain = {
	USA_establish_personal_communication_with_former_naval_person = {

		icon = generic_political_discourse

		allowed = {
			original_tag = USA
		}

		available = {
			has_country_leader = { ruling_only = yes name = "Franklin Delano Roosevelt" }
			has_country_flag = blood_toil_tears_sweat_speech
			ENG = { has_defensive_war = yes }
		}

		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 60 }
			}
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 99 }
			}
		}

		visible = {
			has_war = no
		}

		complete_effect = {
			add_war_support = 0.05
			add_opinion_modifier = { target = ENG modifier = USA_special_relationship }
			reverse_add_opinion_modifier = { target = ENG modifier = USA_special_relationship }
		}
	}

	USA_battle_domestic_isolationism = {

		icon = generic_civil_support

		allowed = {
			original_tag = USA
		}

		available = {
			has_country_leader = { ruling_only = yes name = "Franklin Delano Roosevelt" }
			has_country_flag = fight_on_the_beaches_speech
			ENG = { has_defensive_war = yes }
		}

		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 60 }
			}
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 99 }
			}
		}

		modifier = {
			war_support_weekly = 0.002		
		}

		days_remove = 180

		visible = {
			has_war = no
		}

		complete_effect = {
			add_war_support = 0.05
		}
	}

	USA_emergency_arms_deliveries = {

		icon = generic_prepare_civil_war

		allowed = {
			original_tag = USA
		}

		available = {
			has_country_leader = { ruling_only = yes name = "Franklin Delano Roosevelt" }
			has_country_flag = this_was_their_finest_hour_speech
			ENG = { has_defensive_war = yes }
		}

		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 60 }
			}
			modifier = {
				factor = 10
				has_opinion = { target = ENG value > 99 }
			}
		}

		modifier = {
			war_support_weekly = 0.005		
		}

		days_remove = 30

		visible = {
			has_war = no
		}

		complete_effect = {
			ENG = {
				add_equipment_to_stockpile = { type = infantry_equipment_0 amount = 20000 producer = USA }
				add_equipment_to_stockpile = { type = artillery_equipment_1 amount = 300 producer = USA }
				add_equipment_to_stockpile = { type = support_equipment_1 amount = 300 producer = USA }
				country_event = { id = wtt_britain.30 }
			}
		}
	}

	USA_arsenal_of_democracy_decision = {

		icon = generic_industry

		allowed = {
			original_tag = USA
		}

		available = {
			has_country_leader = { ruling_only = yes name = "Franklin Delano Roosevelt" }
			has_country_flag = mers_el_kebir_raid
			ENG = { has_defensive_war = yes }
		}

		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 10
		}

		visible = {
			has_war = no
		}

		modifier = {
			war_support_weekly = 0.001		
			industrial_capacity_factory = 0.02
			industrial_capacity_dockyard = 0.02
			consumer_goods_factor = -0.05
		}

		days_remove = 360

		complete_effect = {
		}
	}
}

foreign_support = {
	USA_support_the_anti_fascist_war = {

		icon = generic_industry

		allowed = {
			original_tag = USA
		}

		available = {
			NOT = {
				has_global_flag = USA_support_the_anti_fascist_war_flag
			}
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_war_with = JAP
			}
		}

		targets = { CHI PRC GXC YUN SHX XSM SIK D01 D02 D03 D04 D05 D06 D07 D08 D09 D10 D11 D12 D13 D14 D15 }

		target_root_trigger = {
		}

		target_trigger = {
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_war_with = JAP
			}
		}

		visible = {
			OR = { has_government = liberalism has_government = conservatism has_government = social_democracy }
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_war_with = JAP
			}
		}

		cost = 25
		ai_will_do = {
			factor = 50
			modifier = {
				factor = 3
				any_other_country = {
					has_completed_focus = CHI_mission_to_the_us
					has_idea = CHI_soong_mei_ling
					surrender_progress > 0.2
					has_defensive_war = yes
				}
			}

			modifier = {
				factor = 0.5
				has_war_support > 0.6
			}

			modifier = {
				factor = 2
				has_war_with = JAP
			}
		}

		fire_only_once = no

		modifier = {
			civilian_factory_use = 5			
		}

		days_remove = 180

		complete_effect = {
			FROM = {
				add_offsite_building = { type = arms_factory level = 3 }
				set_global_flag = USA_support_the_anti_fascist_war_flag
			}
		}

		remove_effect = {
			add_war_support = 0.05
			FROM = {
				add_offsite_building = { type = arms_factory level = -3 }
				hidden_effect = {
					clr_global_flag = USA_support_the_anti_fascist_war_flag
				}
			}
		}
	}

	USA_guns_for_the_anti_fascist_war = {

		icon = generic_prepare_civil_war

		allowed = {
			original_tag = USA
		}

		available = {
			has_equipment = { infantry_equipment > 999 }
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_idea = CHI_soong_mei_ling
				has_war_with = JAP
			}
		}

		targets = { CHI PRC GXC YUN SHX XSM SIK D01 D02 D03 D04 D05 D06 D07 D08 D09 D10 D11 D12 D13 D14 D15 }

		target_trigger = {
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_idea = CHI_soong_mei_ling
				has_war_with = JAP
			}
		}

		visible = {
			OR = { has_government = liberalism has_government = conservatism has_government = social_democracy }
			FROM = {
				has_completed_focus = CHI_mission_to_the_us
				has_idea = CHI_soong_mei_ling
				has_war_with = JAP
			}
		}

		cost = 25
		ai_will_do = {
			factor = 25
			modifier = {
				factor = 3
				FROM = {
					has_completed_focus = CHI_mission_to_the_us
					has_idea = CHI_soong_mei_ling
					surrender_progress > 0.2
					has_defensive_war = yes
				}
			}

			modifier = {
				factor = 0.5
				has_war_support < 0.6
			}

			modifier = {
				factor = 2
				has_war_with = JAP
			}
		}

		fire_only_once = no	

		days_re_enable = 180

		complete_effect = {
			ROOT = {
				send_equipment = {
					target = FROM
					type = infantry_equipment
					amount = 1000
					old_prioritised = yes
				}
			}
		}
	}
}
USA_foreign_support = {
}

USA_honor_the_confederacy = {
	USA_celebrate_montgomery_convention_day = {
		icon = generic_nationalism
		available = {
			
		}
		cost = 25
		ai_will_do = {
			factor = 1
		}
		fire_only_once = yes
		complete_effect = {
			add_stability = 0.05
			add_popularity = { ideology = radical_nationalism popularity = 0.05 }
			set_cosmetic_tag = USA_CSA
			if = {
				limit = {
					not = {
						has_country_flag = USA_CSA_legitimacy
					}
				}
				set_country_flag = {
					flag = USA_CSA_legitimacy
					value = 1
				}
			}
			else = {
				modify_country_flag = {
					flag = USA_CSA_legitimacy
					value = 1
				}
			}
		}
	}
	USA_move_government_to_richmond = {
		icon = generic_nationalism
		available = {
			362 = {
				is_controlled_by = ROOT
			}
		}
		visible = {
			capital_scope = {
				not = { state = 362 }
			}
		}
		cost = 25
		ai_will_do = {
			factor = 1
		}
		fire_only_once = yes
		complete_effect = {
			add_stability = 0.1
			set_capital = 362
			add_popularity = { ideology = radical_nationalism popularity = 0.05 }
			if = {
				limit = {
					not = {
						has_country_flag = USA_CSA_legitimacy
					}
				}
				set_country_flag = {
					flag = USA_CSA_legitimacy
					value = 1
				}
			}
			else = {
				modify_country_flag = {
					flag = USA_CSA_legitimacy
					value = 1
				}
			}
		}
	}
	USA_secure_state_rights = {
		icon = generic_nationalism
		available = {
			has_cosmetic_tag = USA_CSA 
		}
		visible = {
			
		}
		cost = 75
		fire_only_once = yes
		ai_will_do = {
			factor = 1
		}
		complete_effect = {
			add_stability = 0.1
			add_popularity = { ideology = radical_nationalism popularity = 0.05 }
			if = {
				limit = {
					not = {
						has_country_flag = USA_CSA_legitimacy
					}
				}
				set_country_flag = {
					flag = USA_CSA_legitimacy
					value = 1
				}
			}
			else = {
				modify_country_flag = {
					flag = USA_CSA_legitimacy
					value = 1
				}
			}
		}
	}
	USA_permit_confederate_flags = {
		icon = generic_nationalism
		available = {
			has_cosmetic_tag = USA_CSA
		}
		visible = {
			has_country_flag = {
				flag = USA_CSA_legitimacy
				value > 2
			}
		}
		cost = 75
		fire_only_once = yes
		ai_will_do = {
			factor = 1
		}
		complete_effect = {
			add_stability = 0.1
			add_war_support = 0.1
			add_popularity = { ideology = radical_nationalism popularity = 0.05 }
			if = {
				limit = {
					not = {
						has_country_flag = USA_CSA_legitimacy
					}
				}
				set_country_flag = {
					flag = USA_CSA_legitimacy
					value = 1
				}
			}
			else = {
				modify_country_flag = {
					flag = USA_CSA_legitimacy
					value = 1
				}
			}
		}
	}
	USA_constitutional_convention = {
		icon = eng_trade_unions_support
		available = {
			radical_nationalism > 0.35
		}
		visible = {
			has_country_flag = {
				flag = USA_CSA_legitimacy
				value > 2
			}
			has_cosmetic_tag = USA_CSA
		}
		cost = 100
		fire_only_once = yes
		ai_will_do = {
			factor = 1
		}
		complete_effect = {
			add_stability = 0.1
			add_war_support = 0.05
			add_popularity = { ideology = radical_nationalism popularity = 0.05 }
			set_politics = { ruling_party = radical_nationalism }
		}
	}

}
economy_decisions = {
}

foreign_politics = {
	USA_form_defensive_alliance_north_american_dominion = {

		icon = generic

		available = {
			not = { has_global_flag = mtg_usa_north_american_dominion_refused_faction }
		}

		cost = 25
		fire_only_once = yes
		ai_will_do = {
			factor = 10
			modifier = {
				has_war_with = USA
				factor = 20
			}
		}

		visible = {
			has_country_flag = north_american_dominion_refused
			is_in_faction = no
			not = { has_global_flag = mtg_usa_north_american_dominion_refused_faction }
		}

		complete_effect = {
			set_global_flag = mtg_usa_north_american_dominion_refused_faction
			set_rule = {
				can_create_factions = yes
			}
			create_faction = MTG_USA_NORTH_AMERICAN_DOMINION_FACTION
			hidden_effect = {
				#news_event = { id = xxx days = 3 } MTG_TODO_GABRIEL
			}
		}
	}
	USA_join_defensive_alliance_north_american_dominion = {

		icon = generic

		available = {
			has_global_flag = mtg_usa_north_american_dominion_refused_faction
		}

		cost = 25
		fire_only_once = yes
		ai_will_do = {
			factor = 10
			modifier = {
				has_war_with = USA
				factor = 20
			}
		}

		visible = {
			has_country_flag = north_american_dominion_refused
			is_in_faction = no
		}

		complete_effect = {
			random_other_country = {
				limit = {
					has_country_flag = north_american_dominion_refused
					is_faction_leader = yes
				}
				country_event = generic.2
			}
		}
	}
}