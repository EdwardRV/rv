KOR_leadership_question_category = {
	KOR_seek_IWW_support = {
	
		icon = IWW_icon
		
		cost = 15
		
		days_re_enable = 15
		
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 0
				OR = {
					has_country_flag = KOR_ai_lyuh
					has_country_flag = KOR_ai_kim
				}
			}
		}
		
		complete_effect = {
			KOR_add_council_support_10 = yes
			add_manpower = 10000
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = 500
				producer = SPR
			}
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = 500
				producer = USA
			}
		}
	}
	KOR_seek_soviet_support = {
	
		icon = soviet_flag
		
		visible = {
			NOT = {
				OR = {
					has_war_with = SOV
					has_country_flag = KOR_lyuh_resigned
				}
			}
		}
		
		cost = 15
		
		days_re_enable = 15
		
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 0
				OR = {
					has_country_flag = KOR_ai_anarchism
					has_country_flag = KOR_ai_kim
				}
			}
		}
		
		complete_effect = {
			KOR_add_lyuh_support_10 = yes
			random_owned_controlled_state = {
				add_extra_state_shared_building_slots = 2
				random_state = {
					limit = {
						is_owned_by = ROOT
						is_controlled_by = ROOT
						NOT = {
							state = PREV
						}
					}
					add_extra_state_shared_building_slots = 2
					random_state = {
						limit = {
							is_owned_by = ROOT
							is_controlled_by = ROOT
							NOT = {
								OR = {
									state = PREV
									state = PREV.PREV
								}
							}
						}
						add_extra_state_shared_building_slots = 2
					}
				}
			}
		}
	}
	KOR_seek_PRC_support = {
	
		icon = comchinese_flag
		
		visible = {
			NOT = {
				has_war_with = PRC
			}
		}
		
		cost = 15
		
		days_re_enable = 15
		
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 0
				OR = {
					has_country_flag = KOR_ai_anarchism
					has_country_flag = KOR_ai_lyuh
				}
			}
		}
		
		complete_effect = {
			KOR_add_kim_support_10 = yes
			random_owned_controlled_state = {
				limit = {
					any_neighbor_state = {
						is_owned_by = PRC
					}
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
				}
				add_extra_state_shared_building_slots = 1
				random_list = {
					50 = {
						add_building_construction = {
							type = industrial_complex
							level = 1
							instant_build = yes
						}
					}
					50 = {
						add_building_construction = {
							type = arms_factory
							level = 1
							instant_build = yes
						}
					}
				}
			}
		}
	}
	KOR_push_towards_organisation = {
		
		icon = hol_draw_up_staff_plans
		cost = 15
		
		days_re_enable = 15
		
		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				has_country_flag = KOR_ai_anarchism
			}
		}
		
		complete_effect = {
			KOR_reduce_council_support_15 = yes
			add_stability = 0.15
		}
	}
	KOR_industrialisation_campaign = {
		
		icon = generic_industry 
		
		cost = 15
		
		days_re_enable = 15
		
		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				has_country_flag = KOR_ai_lyuh
			}
		}
		
		complete_effect = {
			if = {
				limit = {
					NOT = {
						has_country_flag = KOR_lyuh_resigned
					}
				}
				KOR_reduce_lyuh_support_15 = yes
			}
			random_owned_controlled_state = {
				limit = {
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
					is_core_of = ROOT
				}
				add_extra_state_shared_building_slots = 1
				random_list = {
					50 = {
						add_building_construction = {
							type = industrial_complex
							level = 1
							instant_build = yes
						}
					}
					50 = {
						add_building_construction = {
							type = arms_factory
							level = 1
							instant_build = yes
						}
					}
				}
			}
		}
	}
	KOR_accept_chinese_refugees = {
		
		icon = eng_blackshirt_march
		
		cost = 15
		
		days_re_enable = 15
		
		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				has_country_flag = KOR_ai_kim
			}
		}
		
		complete_effect = {
			KOR_reduce_kim_support_15 = yes
			add_manpower = 25000
			add_stability = -0.05
		}
	}
}
KOR_jeju_war = {
	KOR_jeju_war_white_peace = {                        
		allowed = { 
			tag = KOR
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = KOR_not_controls_korea
				hidden_trigger = {
					NOT = {
						AND = {
							controls_state = 525
							controls_state = 527
							controls_state = 887
							controls_state = 888
							controls_state = 889
							controls_state = 839
							controls_state = 891
							controls_state = 834
							OR = {
								controls_state = 812
								NOT = {
									owns_state = 812
								}
							}
						}
					}
				}
			}
		}
		activation = {
			has_war_with = JPN
			has_global_flag = KOR_jeju_war
			NOT = {
				has_global_flag = KOR_jeju_war_over
			}
			controls_state = 887
		}
		is_good = no
		days_mission_timeout = 150
		timeout_effect = {
			country_event = rvKorea.30
		}
		complete_effect = {
		}
	}
	JAP_jeju_war_white_peace = {                        
		allowed = { 
			tag = JPN
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = JAP_not_controls_korea
				hidden_trigger = {
					NOT = {
						OR = {
							controls_state = 525
							controls_state = 527
							controls_state = 887
							controls_state = 888
							controls_state = 889
							controls_state = 839
							controls_state = 891
							controls_state = 834
							AND = {
								controls_state = 812
								NOT = {
									KOR = {
										owns_state = 812
									}
								}
							}
						}
					}
				}
			}
		}
		activation = {
			has_war_with = KOR
			has_global_flag = KOR_jeju_war
			NOT = {
				has_global_flag = KOR_jeju_war_over
			}
			NOT = {
				controls_state = 887
			}
		}
		is_good = no
		days_mission_timeout = 150
		timeout_effect = {
			country_event = rvKorea.32
		}
		complete_effect = {
		}
	}
}
operations = {
	KOR_unite_koreans = {
		icon = border_war
		allowed = {
			original_tag = KOR
		}

		available = {
			NOT = {
				has_war_with = PRC
				is_in_faction_with = PRC
			}
			873 = {
				any_neighbor_state = {
					is_controlled_by = ROOT
				}
			}
		}

		visible = {
			NOT = {
				owns_state = 873
			}
			has_completed_focus = KOR_korea_for_all_koreans
		}

		highlight_states = {
			OR = {
				state = 888
				state = 873
			}
		}
		fire_only_once = no
		ai_will_do = {
			factor = 100
		}
		cost = 50
		days_remove = 30
		days_re_enable = 365
		war_with_on_remove = PRC
		complete_effect = {
			888 = {
				save_event_target_as = KOR_PRC_attacker_state
				set_state_flag = border_incident_active
			}
			873 = {
				save_event_target_as = KOR_PRC_defender_state
				set_state_flag = border_incident_active
			}
			PRC = {
				country_event = { id = rvKorea.39 days = 1 }
				activate_targeted_decision = { target = KOR decision = KOR_border_conflict_warning_PRC }
			}
		}
		remove_effect = {
			activate_targeted_decision = { target = PRC decision = KOR_border_incident_forgotten }
			activate_targeted_decision = { target = PRC decision = KOR_escalate_incident_to_border_conflict_PRC }
		}
	}
	KOR_border_conflict_warning_PRC = {

		icon = border_war

		allowed = { always = no } #Activated from effect

		available = {
			hidden_trigger = { always = no }
		}

		war_with_target_on_timeout = yes
		ai_will_do = {
			base = 0
			modifier = {
				
			}
		}

		days_mission_timeout = 30

		fire_only_once = yes

		is_good = no
		
		timeout_effect = {
			custom_effect_tooltip = escalation_possible_tooltip_for_defender
			custom_effect_tooltip = effects_if_border_conflict_is_lost
			effect_tooltip = {
				KOR = {
					transfer_state = 873
				}
			}
			hidden_effect = {
				remove_targeted_decision = { target = KOR decision = KOR_border_conflict_warning_PRC }
				activate_targeted_decision = { target = KOR decision = KOR_border_conflict_escalation_warning_PRC }
			}
		}
	}
	KOR_border_conflict_escalation_warning_PRC = {

		icon = border_war

		allowed = { always = no } #Activated from effect

		available = {
			hidden_trigger = { always = no }
		}

		highlight_states = {
		}

		war_with_target_on_timeout = yes
		ai_will_do = {
			base = 0
			modifier = {
				
			}
		}

		days_mission_timeout = 30

		fire_only_once = yes

		is_good = no

		timeout_effect = {
			custom_effect_tooltip = from_cannot_escalate
			custom_effect_tooltip = effects_if_border_conflict_is_lost
			effect_tooltip = {
				KOR = {
					transfer_state = 873
				}
			}
		}		
	}
	#Mission for attacker to escalate or incident is forgotten
	KOR_border_incident_forgotten = {

		icon = border_war

		allowed = { always = no } #Activated from effect

		available = {
			hidden_trigger = { always = no }
		}

		highlight_states = {
			OR = {
				state = 888
				state = 873
			}
		}

		ai_will_do = {
			base = 0
			modifier = {
				
			}
		}

		days_mission_timeout = 30

		fire_only_once = yes

		is_good = no
		
		timeout_effect = {
			remove_targeted_decision = { target = PRC decision = KOR_escalate_incident_to_border_conflict_PRC }
			add_political_power = -100
			add_war_support = -0.1 #Shameful Display
			hidden_effect = {
				PRC = { remove_targeted_decision = { target = KOR decision = KOR_border_conflict_escalation_warning_PRC } }
				888 = {
					clr_state_flag = border_incident_active
				}
				873 = {
					clr_state_flag = border_incident_active
				}
			}
		}
	}
	KOR_escalate_incident_to_border_conflict_PRC = {

		icon = border_war

		allowed = { always = no } #Activated from effect

		available = {
			888 = {
				OR = {
					is_fully_controlled_by = ROOT
					CONTROLLER = {
						is_subject_of = ROOT
					}
					CONTROLLER = {
						is_in_faction_with = ROOT
					}
				}
			}
			divisions_in_border_state = {
				state = 888
				border_state = 873
				size > 0
			}
		}

		highlight_states = {
			OR = {
				state = 888
				state = 873		
			}
		}

		ai_will_do = {
			base = 10
			modifier = { 
				has_war = no
				factor = 10
			}
		}

		fire_only_once = yes #Reactivation handled in effect

		complete_effect = {
			remove_targeted_decision = { target = PRC decision = KOR_border_incident_forgotten }
			start_border_war = {
				change_state_after_war = no
				attacker = {
					state = 888
					num_provinces = 4
					on_win = rvKorea.40
					on_lose = rvKorea.41
				}
				
				defender = {
					state = 873
					num_provinces = 4
					on_win = rvKorea.41
					on_lose = rvKorea.40
				}
			}
			PRC = {
				activate_targeted_decision = { target = KOR decision = KOR_border_conflict_escalate_conflict }
				activate_targeted_decision = { target = KOR decision = KOR_border_conflict_time_until_cancelled }
			}
			activate_targeted_decision = { target = PRC decision = KOR_border_conflict_escalate_conflict }
			activate_targeted_decision = { target = PRC decision = KOR_border_conflict_time_until_cancelled }
		}
	}
	#Mission for attacker to win border war against FROM in X days
	KOR_border_conflict_time_until_cancelled = {

		icon = border_war

		allowed = { always = no } #Activated from effect

		available = {
			888 = { has_border_war = no }
			873 = { has_border_war = no }
		}
		
		highlight_states = {
			OR = {
				state = 888
				state = 873
			}
		}

		ai_will_do = {
			base = 0
			modifier = {
				
			}
		}

		days_mission_timeout = 100
		is_good = no

		fire_only_once = yes #Reactivation handled in effect

		complete_effect = {

		}

		timeout_effect = {
			cancel_border_war = {
				defender = 873
				attacker = 888
			}
			hidden_effect = {
				remove_targeted_decision = { target = PRC decision = KOR_border_conflict_escalate_conflict }
				remove_targeted_decision = { target = KOR decision = KOR_border_conflict_escalate_conflict }
			}
		}
	}

	#Decision to escalate conflict further
	KOR_border_conflict_escalate_conflict = {

		icon = decision_generic_ignite_civil_war

		allowed = { always = no } #Activated from effect

		visible = {
			has_border_war_with = FROM
		}

		highlight_states = {
			OR = {
				state = 888
				state = 873
			}
		}

		cost = 200

		ai_will_do = {
			base = 0
			modifier = {
				
			}
		}

		fire_only_once = yes #Reactivation handled in effect

		complete_effect = {
			if = {
				limit = { is_border_conflict_defender_vs_FROM = yes }
				set_border_war_data = {
					attacker = 888
					defender = 873
					defender_modifier = 0.15
					combat_width = 100
				}
			}
			else = {
				set_border_war_data = {
					attacker = 888
					defender = 873
					attacker_modifier = 0.15
					combat_width = 100
				}
			}
			PRC = {
				remove_targeted_decision = { target = KOR decision = KOR_border_conflict_escalate_conflict }
			}
		}
	}
}
KOR_colonial_liberation_category = {
	KOR_send_arms_to_indonesia = {
	
		icon = generic_civil_support
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_equipment = {
				infantry_equipment > 1000
			}
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -1000
			}
			set_country_flag = KOR_indonesia_arms
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_send_men_to_indonesia = {
	
		icon = generic_construction
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_manpower > 5000
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_manpower = -5000
			set_country_flag = KOR_indonesia_men
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_send_arms_to_kalimantan = {
	
		icon = generic_prepare_civil_war
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_equipment = {
				infantry_equipment > 1000
			}
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -1000
			}
			set_country_flag = KOR_kalimantan_arms
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_send_men_to_kalimantan = {
	
		icon = generic_operation
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_manpower > 5000
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_manpower = -5000
			set_country_flag = KOR_kalimantan_men
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_send_arms_to_sulawesi = {
	
		icon = generic_army_support
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_equipment = {
				infantry_equipment > 1000
			}
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -1000
			}
			set_country_flag = KOR_sulawesi_arms
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_send_men_to_sulawesi = {
	
		icon = generic_ignite_civil_war
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_manpower > 5000
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_manpower = -5000
			set_country_flag = KOR_sulawesi_men
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_point_them_to_the_wrong_direction = {
	
		icon = generic_political_discourse
		
		cost = 50
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_country_flag = KOR_indonesia_arms
			has_country_flag = KOR_indonesia_men
			has_country_flag = KOR_kalimantan_arms
			has_country_flag = KOR_kalimantan_men
			has_country_flag = KOR_sulawesi_arms
			has_country_flag = KOR_sulawesi_men
		}
		
		visible = {
			INS = {
				is_ai = yes
			}
		}
		
		days_remove = 20
		
		complete_effect = {
		}
		
		remove_effect = {
			INS = {
				country_event = rvKorea.42
			}
			set_country_flag = KOR_INS_distracted
		}
	}
	KOR_great_insulindian_uprising = {
	
		icon = generic_nationalism
		
		cost = 50
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_country_flag = KOR_indonesia_arms
			has_country_flag = KOR_indonesia_men
			has_country_flag = KOR_kalimantan_arms
			has_country_flag = KOR_kalimantan_men
			has_country_flag = KOR_sulawesi_arms
			has_country_flag = KOR_sulawesi_men
			if = {
				limit = {
					INS = {
						is_ai = yes
					}
				}
				has_country_flag = KOR_INS_distracted
			}
		}
		
		visible = {
		}
		
		days_remove = 20
		
		complete_effect = {
		}
		
		remove_effect = {
			INS = {
				country_event = rvKorea.43
			}
		}
	}
	KOR_invite_insulindia = {
	
		icon = generic_operation
		
		cost = 0
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_global_flag = KOR_INS_uprising_success
		}
		
		visible = {
			has_global_flag = KOR_INS_uprising_success
		}
		
		complete_effect = {
			SMT = {
				country_event = rvKorea.49
			}
			KLM = {
				country_event = rvKorea.49
			}
			SLW = {
				country_event = rvKorea.49
			}
		}
	}
	KOR_send_arms_to_north_papua = {
	
		icon = generic_prepare_civil_war
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_equipment = {
				infantry_equipment > 1000
			}
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		visible = {
			has_completed_focus = KOR_arm_the_papuans
			has_global_flag = KOR_INS_uprising_success
		}
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -1000
			}
			set_country_flag = KOR_north_papua_arms
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_send_men_to_north_papua = {
	
		icon = generic_army_support
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_manpower > 5000
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		visible = {
			has_completed_focus = KOR_arm_the_papuans
			has_global_flag = KOR_INS_uprising_success
		}
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_manpower = -5000
			set_country_flag = KOR_north_papua_men
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_send_arms_to_solomon = {
	
		icon = generic_ignite_civil_war
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_equipment = {
				infantry_equipment > 1000
			}
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		visible = {
			has_completed_focus = KOR_arm_the_papuans
			has_global_flag = KOR_INS_uprising_success
		}
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -1000
			}
			set_country_flag = KOR_solomon_arms
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_send_men_to_solomon = {
	
		icon = generic_construction
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_manpower > 5000
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		visible = {
			has_completed_focus = KOR_arm_the_papuans
			has_global_flag = KOR_INS_uprising_success
		}
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_manpower = -5000
			set_country_flag = KOR_solomon_men
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_papua_uprising = {
	
		icon = generic_nationalism
		
		cost = 50
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_country_flag = KOR_north_papua_arms
			has_country_flag = KOR_north_papua_men
			has_country_flag = KOR_solomon_arms
			has_country_flag = KOR_solomon_men
		}
		
		visible = {
			has_completed_focus = KOR_arm_the_papuans
			has_global_flag = KOR_INS_uprising_success
		}
		
		days_remove = 20
		
		complete_effect = {
		}
		
		remove_effect = {
			AST = {
				country_event = rvKorea.46
			}
		}
	}
	KOR_invite_papua = {
	
		icon = generic_operation
		
		cost = 0
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_global_flag = KOR_PNG_uprising_success
		}
		
		visible = {
			has_global_flag = KOR_PNG_uprising_success
		}
		
		complete_effect = {
			PNG = {
				country_event = rvKorea.49
			}
			SOL = {
				country_event = rvKorea.49
			}
		}
	}
	KOR_send_arms_to_malaya = {
	
		icon = generic_prepare_civil_war
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_equipment = {
				infantry_equipment > 1000
			}
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		visible = {
			has_completed_focus = KOR_arm_the_malayans
			has_global_flag = KOR_PNG_uprising_success
		}
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -1000
			}
			set_country_flag = KOR_malaya_arms
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_send_men_to_malaya = {
	
		icon = generic_army_support
		
		cost = 25
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_manpower > 5000
			NOT = {
				has_country_flag = KOR_sending_stuff
			}
		}
		
		visible = {
			has_completed_focus = KOR_arm_the_malayans
			has_global_flag = KOR_PNG_uprising_success
		}
		days_remove = 20
		
		complete_effect = {
			set_country_flag = KOR_sending_stuff
		}
		
		remove_effect = {
			add_manpower = -5000
			set_country_flag = KOR_malaya_men
			clr_country_flag = KOR_sending_stuff
		}
	}
	KOR_malaya_uprising = {
	
		icon = generic_nationalism
		
		cost = 50
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_country_flag = KOR_malaya_arms
			has_country_flag = KOR_malaya_men
		}
		
		visible = {
			has_completed_focus = KOR_arm_the_malayans
			has_global_flag = KOR_PNG_uprising_success
		}
		
		days_remove = 20
		
		complete_effect = {
		}
		
		remove_effect = {
			MAL = {
				country_event = rvKorea.52
			}
		}
	}
	KOR_invite_malaya = {
	
		icon = generic_operation
		
		cost = 0
		
		fire_only_once = yes
		
		ai_will_do = {
			factor = 20
		}
		
		available = {
			has_global_flag = KOR_MLY_uprising_success
		}
		
		visible = {
			has_global_flag = KOR_MLY_uprising_success
		}
		
		complete_effect = {
			MLY = {
				country_event = rvKorea.49
			}
		}
	}
}