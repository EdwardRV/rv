political_actions = {

	CZE_int_slovakia_1 = {

		allowed = {
			original_tag = CZE
		}
		
		cost = 100
		
		available = {
			NOT = {
				owns_state = 70
				has_full_control_of_state = 70
			}
			has_completed_focus = CZE_reintegrate_slovaks
		}

		ai_will_do = {
			factor = 200
		}

		visible = {
			NOT = { has_full_control_of_state = 70 }
			has_completed_focus = CZE_reintegrate_slovaks
		}
		fire_only_once = yes
		complete_effect = {
			
		}
		days_remove = 70
		remove_effect = {
			add_state_core = 70
		}
	}
	
	CZE_int_slovakia_2 = {

		allowed = {
			original_tag = CZE
		}
		
		cost = 100

		available = {
			NOT = {
				owns_state = 71
				has_full_control_of_state = 71
			}
			has_completed_focus = CZE_reintegrate_slovaks
		}

		ai_will_do = {
			factor = 200
		}

		visible = {
			NOT = { has_full_control_of_state = 71 }
			has_completed_focus = CZE_reintegrate_slovaks
		}
		fire_only_once = yes
		complete_effect = {
			
		}
		days_remove = 70
		remove_effect = {
			add_state_core = 71
		}
	}
	
	CZE_int_slovakia_3 = {

		allowed = {
			original_tag = CZE
		}
		
		cost = 100

		available = {
			NOT = {
				owns_state = 73
				has_full_control_of_state = 73
			}
			has_completed_focus = CZE_reintegrate_slovaks
		}

		ai_will_do = {
			factor = 200
		}

		visible = {
			NOT = { has_full_control_of_state = 73 }
			has_completed_focus = CZE_reintegrate_slovaks
		}
		fire_only_once = yes
		complete_effect = {
			
		}
		days_remove = 70
		remove_effect = {
			add_state_core = 73
		}
	}
	
	CZE_int_slovakia_4 = {

		allowed = {
			original_tag = CZE
		}
		
		cost = 100

		available = {
			NOT = {
				owns_state = 664
				has_full_control_of_state = 664
			}
			has_completed_focus = CZE_reintegrate_slovaks
		}

		ai_will_do = {
			factor = 200
		}

		visible = {
			NOT = { has_full_control_of_state = 664 }
			has_completed_focus = CZE_reintegrate_slovaks
		}
		fire_only_once = yes
		complete_effect = {
			
		}
		days_remove = 70
		remove_effect = {
			add_state_core = 664
		}
	}
	
	CZE_improve_CS_relations = {

		allowed = {
			original_tag = CZE
		}
		
		cost = 50

		available = {
			NOT = {
				owns_state = 70
				has_full_control_of_state = 70
				owns_state = 71
				has_full_control_of_state = 71
				owns_state = 73
				has_full_control_of_state = 73
				owns_state = 664
				has_full_control_of_state = 664
				has_war_with = SLO
			}
			has_completed_focus = CZE_reintegrate_slovaks
			country_exists = SLO
		}

		ai_will_do = {
			factor = 200
		}

		visible = {
			NOT = { 
				has_full_control_of_state = 70
				has_full_control_of_state = 71
				has_full_control_of_state = 73
				has_full_control_of_state = 664 
			}
			has_completed_focus = CZE_reintegrate_slovaks
			country_exists = SLO
		}
		fire_only_once = no
		complete_effect = {
			
		}
		days_remove = 35
		remove_effect = {
			add_opinion_modifier = {
				target = SLO
				modifier = medium_increase 
			}
			SLO = {
				add_opinion_modifier = {
					target = CZE
					modifier = medium_increase 
				}
			}
		}
	}
	
	CZE_annex_SLO = {

		allowed = {
			original_tag = CZE
		}
		
		cost = 200

		available = {
			70 = {
				is_core_of = CZE
			}
			71 = {
				is_core_of = CZE
			}
			73 = {
				is_core_of = CZE
			}
			664 = {
				is_core_of = CZE
			}
			has_completed_focus = CZE_reintegrate_slovaks
			country_exists = SLO
			SLO = {
				has_opinion = {
					target = CZE
					value > 99
				}
			}
		}

		ai_will_do = {
			factor = 200
		}

		visible = {
			NOT = { 
				has_full_control_of_state = 70
				has_full_control_of_state = 71
				has_full_control_of_state = 73
				has_full_control_of_state = 664 
			}
			has_completed_focus = CZE_reintegrate_slovaks
			country_exists = SLO
		}
		fire_only_once = no
		complete_effect = {
			
		}
		days_remove = 100
		remove_effect = {
			annex_country = {
				target = SLO
				transfer_troops = yes
			}
		}
	}
}