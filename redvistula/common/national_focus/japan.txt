focus_tree = {
	id = japan_focus_tree

	country = {
		factor = 0
		modifier = {
			add = 20
			tag = JAP
		}
	}
	shared_focus = JAP_army_reform
	shared_focus = JAP_new_naval_estimates
	shared_focus = JAP_civil_war
	shared_focus = JAP_civil_war_victory
	default = no
	continuous_focus_position = { x = 100 y = 800 }
}
shared_focus = {
	id = JAP_civil_war_victory
	icon = GFX_focus_JAP_civil_war_victory_toseiha
	x = 32
	y = 5

	cost = 8

	ai_will_do = {
		factor = 100
	}

	available = {
		has_global_flag = JAP_civil_war_over
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		add_war_support = 0.1
		add_stability = 0.1
	}
}
shared_focus = {
	id = JAP_prepare_civil_war_recovery
	icon = GFX_focus_JAP_civil_war_recovery_preparation
	x = 0
	y = 1
	relative_position_id = JAP_civil_war_victory
	prerequisite = { focus = JAP_civil_war_victory }

	cost = 8

	ai_will_do = {
		factor = 100
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		custom_effect_tooltip = JAP_unlocks_JCW_recovery_plan_creation_tt
	}
}
shared_focus = {
	id = JAP_war_propaganda
	icon = GFX_focus_JAP_war_propaganda
	x = -1
	y = 1
	relative_position_id = JAP_prepare_civil_war_recovery
	prerequisite = { focus = JAP_prepare_civil_war_recovery }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		swap_ideas = {
			add_idea = JAP_humiliation_in_manchuria_1
			remove_idea = JAP_humiliation_in_manchuria
		}
	}
}
shared_focus = {
	id = JAP_award_civil_war_heroes
	icon = GFX_focus_JAP_award_civil_war_heroes
	x = 1
	y = 1
	relative_position_id = JAP_prepare_civil_war_recovery
	prerequisite = { focus = JAP_prepare_civil_war_recovery }

	cost = 8

	ai_will_do = {
		factor = 100
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		add_war_support = 0.15
		army_experience = 50
	}
}
shared_focus = {
	id = JAP_weaken_civilian_government
	icon = GFX_focus_JAP_weaken_civilian_government
	x = -1
	y = 1
	relative_position_id = JAP_war_propaganda
	prerequisite = { focus = JAP_war_propaganda }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		add_ideas = JAP_military_government
		add_stability = -0.1
		hidden_effect = {
			country_event = {
				id = rv_japan.32
				days = 180
			}
		}
	}
}
shared_focus = {
	id = JAP_military_purges
	icon = GFX_focus_JAP_military_purges
	x = 1
	y = 1
	relative_position_id = JAP_war_propaganda
	prerequisite = { focus = JAP_war_propaganda }
	prerequisite = { focus = JAP_award_civil_war_heroes }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		swap_ideas = {
			add_idea = JAP_military_purged
			remove_idea = JAP_low_military_support
		}
	}
}
shared_focus = {
	id = JAP_crush_korean_nationalism
	icon = GFX_focus_JAP_crush_korean_nationalism
	x = 1
	y = 1
	relative_position_id = JAP_award_civil_war_heroes
	prerequisite = { focus = JAP_award_civil_war_heroes }

	cost = 8

	ai_will_do = {
		factor = 100
	}

	available = {
	
	}

	bypass = {
		hidden_trigger = {
			country_exists = KOR
		}
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no

	select_effect = {
		hidden_effect = {
			country_event = { id = rv_japan.33 days = 35 }
		}
	}
	completion_reward = {
		effect_tooltip = {
			remove_ideas = JAP_korean_separatism
		}
	}
}
shared_focus = {
	id = JAP_begin_the_recovery
	icon = GFX_focus_JAP_begin_civil_war_recovery
	x = -5
	y = 1
	relative_position_id = JAP_civil_war_victory
	prerequisite = { focus = JAP_civil_war_victory }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
		has_country_flag = JAP_has_recovery_plan
		custom_trigger_tooltip = {
			tooltip = JAP_crushed_korean_nationalism_tt
			hidden_trigger = {
				has_completed_focus = JAP_crush_korean_nationalism
			}
		}
		custom_trigger_tooltip = {
			tooltip = JAP_purged_military_tt
			hidden_trigger = {
				has_completed_focus = JAP_military_purges
			}
		}
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		custom_effect_tooltip = JAP_unlocks_recovery_decisions_tt
	}
}
shared_focus = {
	id = JAP_new_naval_commanders
	icon = GFX_focus_JAP_new_naval_commanders
	x = -2
	y = 1
	relative_position_id = JAP_begin_the_recovery
	prerequisite = { focus = JAP_begin_the_recovery }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		if = {
			limit = {
				has_idea = JAP_military_purged_navy
			}
			swap_ideas = {
				add_idea = JAP_military_purged_2
				remove_idea = JAP_military_purged_navy
			}
			else = {
				swap_ideas = {
					add_idea = JAP_military_purged_army
					remove_idea = JAP_military_purged
				}
			}
		}
	}
}
shared_focus = {
	id = JAP_refine_the_recovery_plan
	icon = GFX_focus_JAP_civil_war_recovery_preparation
	x = 0
	y = 1
	relative_position_id = JAP_begin_the_recovery
	prerequisite = { focus = JAP_begin_the_recovery }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_construction_mil_10
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_construction_mil_10
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_construction_mil_20
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_construction_mil_20
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_construction_dock_10
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_construction_dock_10
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_construction_dock_20
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_construction_dock_20
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_construction_civ_10
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_construction_civ_10
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_construction_civ_20
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_construction_civ_20
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_industry_mil_10
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_industry_mil_10
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_industry_mil_20
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_industry_mil_20
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_industry_dock_10
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_industry_dock_10
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_industry_dock_20
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_industry_dock_20
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_industry_civ_10
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_industry_civ_10
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_industry_civ_20
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_industry_civ_20
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_research_mil_10
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_research_mil_10
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_research_mil_20
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_research_mil_20
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_research_dock_10
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_research_dock_10
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_research_dock_20
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_research_dock_20
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_research_civ_10
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_research_civ_10
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_civil_war_recovery_research_civ_20
			}
			modify_timed_idea = {
				idea = JAP_civil_war_recovery_research_civ_20
				days = 365
			}
		}
		if = {
			limit = {
				has_idea = JAP_no_recovery_plan
			}
			modify_timed_idea = {
				idea = JAP_no_recovery_plan
				days = -365
			}
		}
		if = {
			limit = {
				NOT = {
					OR = {
						has_idea = JAP_civil_war_recovery_construction_mil_10
						has_idea = JAP_civil_war_recovery_construction_mil_20
						has_idea = JAP_civil_war_recovery_construction_dock_10
						has_idea = JAP_civil_war_recovery_construction_dock_20
						has_idea = JAP_civil_war_recovery_construction_civ_10
						has_idea = JAP_civil_war_recovery_construction_civ_20
						has_idea = JAP_civil_war_recovery_industry_mil_10
						has_idea = JAP_civil_war_recovery_industry_mil_20
						has_idea = JAP_civil_war_recovery_industry_dock_10
						has_idea = JAP_civil_war_recovery_industry_dock_20
						has_idea = JAP_civil_war_recovery_industry_civ_10
						has_idea = JAP_civil_war_recovery_industry_civ_20
						has_idea = JAP_civil_war_recovery_research_mil_10
						has_idea = JAP_civil_war_recovery_research_mil_20
						has_idea = JAP_civil_war_recovery_research_dock_10
						has_idea = JAP_civil_war_recovery_research_dock_20
						has_idea = JAP_civil_war_recovery_research_civ_10
						has_idea = JAP_civil_war_recovery_research_civ_20
						has_idea = JAP_no_recovery_plan
					}
				}
			}
			custom_effect_tooltip = JAP_extends_recovery_plan_tt
		}
	}
}
shared_focus = {
	id = JAP_new_army_commanders
	icon = GFX_focus_JAP_award_civil_war_heroes
	x = 2
	y = 1
	relative_position_id = JAP_begin_the_recovery
	prerequisite = { focus = JAP_begin_the_recovery }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		if = {
			limit = {
				has_idea = JAP_military_purged_army
			}
			swap_ideas = {
				add_idea = JAP_military_purged_2
				remove_idea = JAP_military_purged_army
			}
			else = {
				swap_ideas = {
					add_idea = JAP_military_purged_navy
					remove_idea = JAP_military_purged
				}
			}
		}
	}
}
shared_focus = {
	id = JAP_build_new_dockyards
	icon = GFX_focus_JAP_build_new_dockyards
	x = 1
	y = 1
	relative_position_id = JAP_new_naval_commanders
	prerequisite = { focus = JAP_new_naval_commanders }
	prerequisite = { focus = JAP_refine_the_recovery_plan }
	prerequisite = { focus = JAP_new_army_commanders }
	mutually_exclusive = { focus = JAP_build_new_military_factories }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = dockyard
					size > 1
					include_locked = yes
				}
				is_core_of = ROOT
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = dockyard
				level = 2
				instant_build = yes
			}
			ROOT = {
				random_owned_state = {
					limit = {
						NOT = {
							state = PREV.PREV
						}
						free_building_slots = {
							building = dockyard
							size > 1
							include_locked = yes
						}
						is_core_of = ROOT
					}
					add_extra_state_shared_building_slots = 2
					add_building_construction = {
						type = dockyard
						level = 2
						instant_build = yes
					}
				}
			}
		}
		JAP_ANB_towards_navy_10 = yes
	}
}
shared_focus = {
	id = JAP_build_new_military_factories
	icon = GFX_focus_JAP_build_new_military_factories
	x = 1
	y = 1
	relative_position_id = JAP_refine_the_recovery_plan
	prerequisite = { focus = JAP_new_naval_commanders }
	prerequisite = { focus = JAP_refine_the_recovery_plan }
	prerequisite = { focus = JAP_new_army_commanders }
	mutually_exclusive = { focus = JAP_build_new_dockyards }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				is_core_of = ROOT
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = arms_factory
				level = 2
				instant_build = yes
			}
			ROOT = {
				random_owned_state = {
					limit = {
						NOT = {
							state = PREV.PREV
						}
						free_building_slots = {
							building = arms_factory
							size > 1
							include_locked = yes
						}
						is_core_of = ROOT
					}
					add_extra_state_shared_building_slots = 2
					add_building_construction = {
						type = arms_factory
						level = 2
						instant_build = yes
					}
				}
			}
		}
		JAP_ANB_towards_army_10 = yes
	}
}
shared_focus = {
	id = JAP_finalize_the_recovery
	icon = GFX_focus_JAP_begin_civil_war_recovery
	x = 5
	y = 1
	relative_position_id = JAP_civil_war_victory
	prerequisite = { focus = JAP_civil_war_victory }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
		custom_trigger_tooltip = {
			tooltip = JAP_all_green_states_tt
			hidden_trigger = {
				all_owned_state = {
					NOT = {
						OR = {
							has_state_flag = JAP_JCW_red_state
							has_state_flag = JAP_JCW_yellow_state
						}
					}
				}
			}
		}
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		custom_effect_tooltip = JAP_unlocks_full_recovery_decisions_tt
	}
}
shared_focus = {
	id = JAP_purge_the_civilian_government
	icon = GFX_focus_JAP_weaken_civilian_government
	x = -1
	y = 1
	relative_position_id = JAP_finalize_the_recovery
	prerequisite = { focus = JAP_finalize_the_recovery }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
		has_idea = JAP_civilian_government_protesting
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		remove_ideas = JAP_civilian_government_protesting
	}
}
shared_focus = {
	id = JAP_recover_the_army
	icon = GFX_focus_JAP_war_propaganda
	x = 1
	y = 1
	relative_position_id = JAP_finalize_the_recovery
	prerequisite = { focus = JAP_finalize_the_recovery }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		remove_ideas = JAP_military_purged_2
	}
}
shared_focus = {
	id = JAP_strengthen_the_army
	icon = GFX_focus_JAP_build_new_military_factories
	x = 0
	y = 1
	relative_position_id = JAP_purge_the_civilian_government
	prerequisite = { focus = JAP_purge_the_civilian_government }
	prerequisite = { focus = JAP_recover_the_army }
	mutually_exclusive = { focus = JAP_strengthen_the_navy }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				is_core_of = ROOT
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = arms_factory
				level = 2
				instant_build = yes
			}
			ROOT = {
				random_owned_state = {
					limit = {
						NOT = {
							state = PREV.PREV
						}
						free_building_slots = {
							building = arms_factory
							size > 1
							include_locked = yes
						}
						is_core_of = ROOT
					}
					add_extra_state_shared_building_slots = 2
					add_building_construction = {
						type = arms_factory
						level = 2
						instant_build = yes
					}
				}
			}
		}
		JAP_ANB_towards_army_10 = yes
	}
}
shared_focus = {
	id = JAP_strengthen_the_navy
	icon = GFX_focus_JAP_build_new_dockyards
	x = 0
	y = 1
	relative_position_id = JAP_recover_the_army
	prerequisite = { focus = JAP_purge_the_civilian_government }
	prerequisite = { focus = JAP_recover_the_army }
	mutually_exclusive = { focus = JAP_strengthen_the_army }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = dockyard
					size > 1
					include_locked = yes
				}
				is_core_of = ROOT
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = dockyard
				level = 2
				instant_build = yes
			}
			ROOT = {
				random_owned_state = {
					limit = {
						NOT = {
							state = PREV.PREV
						}
						free_building_slots = {
							building = dockyard
							size > 1
							include_locked = yes
						}
						is_core_of = ROOT
					}
					add_extra_state_shared_building_slots = 2
					add_building_construction = {
						type = dockyard
						level = 2
						instant_build = yes
					}
				}
			}
		}
		JAP_ANB_towards_navy_10 = yes
	}
}
shared_focus = {
	id = JAP_greater_east_asian_co_prosperity_sphere
	icon = GFX_focus_JAP_greater_east_asia_co_prosperity_sphere
	x = 0
	y = 1
	relative_position_id = JAP_military_purges
	prerequisite = { focus = JAP_build_new_dockyards focus = JAP_build_new_military_factories }
	prerequisite = { focus = JAP_weaken_civilian_government }
	prerequisite = { focus = JAP_military_purges }
	prerequisite = { focus = JAP_crush_korean_nationalism }
	prerequisite = { focus = JAP_strengthen_the_army focus = JAP_strengthen_the_navy }

	cost = 8

	ai_will_do = {
		factor = 3
	}

	available = {
	
	}

	bypass = {
	}

	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no


	completion_reward = {
		create_faction = JAP_greater_east_asian_co_prosperity_sphere
		custom_effect_tooltip = JAP_new_focus_tree_tt
		hidden_effect = {
			if = {
				limit = {
					has_global_flag = KOR_independence_war_victory
				}
				load_focus_tree = { tree = japan_diplomatic_focus_tree_no_korea keep_completed = yes }
			}
			else = {
				load_focus_tree = { tree = japan_diplomatic_focus_tree_korea keep_completed = yes }
			}
		}
	}
}