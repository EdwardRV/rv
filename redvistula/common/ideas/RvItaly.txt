ideas = {
	country = {
		victor_emmanuel = {
			
			
			allowed = {
				original_tag = ITA
			}

			allowed_civil_war = {
				NOT = {
					OR = { has_government = marxism has_government = industrialism has_government = socialism }
				}
				NOT = {
					any_other_country = {
						original_tag = ITA
						OR = { has_government = totalitarianism has_government = radical_nationalism }
					}
				}
			}
			
			removal_cost = -1
			
			modifier = {
				stability_factor = 0.05				
			}
		}
		vittoria_mutilata = {			
			
			allowed = {
				original_tag = ITA
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						OR = { has_government = marxism has_government = industrialism has_government = socialism }
						OR = { has_government = liberalism has_government = conservatism has_government = social_democracy }
					}
				}
			}
			
			removal_cost = -1
			
			modifier = {
				marxism_acceptance = -10
				liberalism_acceptance = -10
				radical_nationalism_acceptance = 10
				ai_focus_aggressive_factor = 0.5
				justify_war_goal_time = -0.05
			}
		}

		ITA_german_millitary_cooperation_focus = {

			picture = german_advisors

			allowed = {
				original_tag = ITA
			}

			allowed_civil_war = {
				OR = {
					AND = {
						OR = { has_government = totalitarianism has_government = radical_nationalism }
						GER = {
							OR = { has_government = totalitarianism has_government = radical_nationalism }
						}
					}
					AND = {
						OR = { has_government = marxism has_government = industrialism has_government = socialism }
						GER = {
							OR = { has_government = marxism has_government = industrialism has_government = socialism }
						}
					}
					AND = {
						OR = { has_government = liberalism has_government = conservatism has_government = social_democracy }
						GER = {
							OR = { has_government = liberalism has_government = conservatism has_government = social_democracy }
						}
					}
				}
			}

			removal_cost = -1

			research_bonus = {
				land_doctrine = 0.05
			}
			research_bonus = {
				air_doctrine = 0.05
			}				
		}

	}
			
			
	political_advisor = {
			
		ivanoe_bonomi = {
			
			
			allowed = {
				original_tag = ITA
			}
			
			available = {
				if = {
					limit = { has_dlc = "Man the Guns" }	
					NOT = { has_autonomy_state = autonomy_supervised_state }
				}
			}
			
			available = {
			}
			
			traits = { democratic_reformer }

			do_effect = {
				NOT = {
					OR = { has_government = liberalism has_government = conservatism has_government = social_democracy }
				}
			}
			
			ai_will_do = {
				factor = 0
			}
		}
		
		amadeo_bordiga = {
			
			
			allowed = {
				original_tag = ITA
			}
			
			available = {
				if = {
					limit = { has_dlc = "Man the Guns" }	
					NOT = { has_autonomy_state = autonomy_supervised_state }
				}
			}
			
			available = {
			}
			
			traits = { communist_revolutionary }

			do_effect = {
				NOT = {
					OR = { has_government = marxism has_government = industrialism has_government = socialism }
				}
			}
			
			ai_will_do = {
				factor = 0
			}
		}
	}
}