#########       BEHOLD, THE POWER OF FRANCE                #########
#########   Based on the Novel "Push" by Sapphire          #########
#########  					Swagmasterr Productions                #########


focus_tree = {
		id = french_focus

		country = {
			factor = 0

			modifier = {
					add = 20
					tag = FRA
			}
		}
	continous_focus_position = { x = 0 y = 1000 }

													#### MONARCHIST PATHLINE #####

			focus = {
					id = fra_coronate_napoleon
					icon = GFX_focus_fra_support_the_monarchists
					x = 4
					y = 0
					cost = 5
			ai_will_do = {
				factor = 10
			}
				completion_reward = {
					add_political_power = 150
				}
		}

		focus = {
			id = fra_letat_cest_moi
			icon = GFX_focus_generic_home_defense
			prerequisite = { focus = fra_coronate_napoleon }
			relative_position_id = fra_coronate_napoleon
			x = 0
			y = 1
			cost = 10
		ai_will_do = {
			factor = 10
		}
			completion_reward = {
					add_popularity = {
						ideology = fascist
						popularity = 0.7
						}
										}
						}

		focus = {
				id = fra_dawnof_thirdempire
				icon = GFX_focus_generic_coastal_fort
				prerequisite = { focus = fra_abolish_third_republic }
		relative_position_id = fra_abolish_third_republic
				x = 0
				y = 1
				cost = 5
		ai_will_do {
			factor = 8
		}
		completion_reward = {
				add_stability = 0.3
		}
		}

		focus = {
			id = fra_abolish_third_republic
			icon = GFX_focus_usa_voter_registration_act
			prerequisite = { focus = fra_letat_cest_moi }
				relative_position_id = fra_letat_cest_moi
				x = 0
				y = 1
				cost = 10
				ai_will_do = {
					factor = 8
				}
				completion_reward = {
				remove_ideas = fra_french_third_republic
				add_ideas = fra_third_french_empire
				}
		}

										### DIPLOMATIC ###

					focus = {
						id = fra_bel_union
						icon = GFX_focus_generic_home_defense
						prerequisite = {focus = fra_letat_cest_moi }
							relative_position_id = fra_letat_cest_moi
						x = -3
						y = 0
							available = {
								NOT = {
									BEL = {
										exists = no
									}
									BEL = {
										is_puppet = yes
									}
									}
					}
					completion_reward = {
						BEL { country_event = { id = Rv.BEL.1 } }
						custom_effect_tooltip = fra_puppet_bel_tooltip
					}
							}

										### INDUSTRIAL ###

					focus = {
						id = fra_revive_french_economy
						icon = GFX_focus_generic_coastal_fort
						prerequisite = { focus = fra_coronate_napoleon }
						relative_position_id = fra_coronate_napoleon
	  				x = 10
	  				y = 1
	    			cost = 10
						ai_will_do = {
						factor = 8
						}
						completion_reward = {
							production_speed_buildings_factor = 0.1
							}
		}

		focus = {
				id = fra_allroads_leadtoparis
				icon = GFX_focus_generic_coastal_fort
				prerequisite = { focus = fra_revive_french_economy }
				relative_position_id = fra_revive_french_economy
			x = 0
			y = 1
		 		cost = 8
				ai_will_do = {
				factor = 10
				bypass = {
						FRA = {
							controls_state = 16
							controls_state = 15
							controls_state = 14
							controls_state = 29
							controls_state = 785
								AND = {
									id ={
										16 = {
											infrastructure > 10
										}
									id = {
										15 = {
											infrastructure > 9
										}
									id = {
										14 =
											infrastructure > 8
										}
									id = {
										29 = {
											infrastructure >
										}
									id = {
										785 = {
											infrasture > 9
										}
									}
									}
										}
									}
									}
								}
						}
				}
				available = {
					original_tag = FRA
						AND = {
								FRA = {
									controls_state = 16
									controls_state = 15
									controls_state = 14
									controls_state = 29
									controls_state = 785
								}
						}
				}
				available_if_capitulated = no
				completion_reward = {
					16 = {
								add_extra_state_shared_building_slots = 3
								set_building_level = {
										type = infrastructure
										level = 10
										instant_build = yes
								}
							}
					15 = {
								set_building_level = {
								type = infrastructure
								level = 9
								instant_build = yes
								limit < 9
					}
				}

					14 = {
								set_building_level = {
								type = infrastructure
								level = 8
								instant_build = yes
								limit < 8
								}
					}

					29 = {
								set_building_level = {
								type = infrastructure
								level = 9
								instant_build = yes
								limit < 9
								}
					}

					785 = {
							  set_building_level = {
								type = infrastructure
								level = 9
								instant_build = yes
								}
					}

					}
				}

				focus = {
					id = fra_eventheones_southofit
					icon = GFX_focus_usa_focus_on_europe
					relative_position_id = fra_all_roads_lead_to_paris
					x = 1
					y = 1
					prerequisite = { focus = fra_all_roads_lead_to_paris }
						FRA =
					cost = 10
					ai_will_do = {
						factor = 8
					}
						completion_reward = {
				24 = {
					set_building_level =  {
					type = infrastructure
					level = 8
					instant_build = yes
					}
				}
				27 = {
					set_building_level = {
					type = infrastructure
					level = 8
					instant_build = yes
					}
				}
			23 = {
				set_building_level = {
					type = infrastructure
					level = 9
					instant_build = yes
				}
			}
			33 = {
				set_building_level = {
					type = infrastructure
					level = 9
					instant_build = yes
				}
			}
				}
						}

		focus = {
				id = fra_city_of_lights
				icon = GFX_goal_generic_radar
		prerequisite = { focus = fra_revive_french_economy }
		relative_position_id = fra_revive_french_economy
				x = 1
				y = 1
			available = {
				FRA = {
						controls_state = 16
					}
					AND = original_tag = FRA
				}
			cost = 10
			completion_reward {
				16 = {
					add_extra_state_shared_building_slots = 5
					add_building_construction = {
						type = industrial_complex
						level = 2
						instant_build = yes
					}
					add_building_construction = {
						type = arms_factory
						level = 3
						instant_build = yes
					}
				}
			}
				}
			}
