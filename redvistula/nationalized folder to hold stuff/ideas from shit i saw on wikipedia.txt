French by birth, heart, reason and will, I shall fulfil the duties of a conscious patriot. I pledge myself to fight against every republican regime. The republican spirit disorganises national defense and favours religious influences directly hostile to traditional Catholicism. A regime that is French must be restored to France. Our only future lies, therefore, in the Monarch, as it is personified in the heir of the forty kings who, for a thousand years, made France. Only the Monarchy ensures public safety and, in its responsibility for order, prevents the public evils that antisemitism and nationalism denounce. The necessary instrument of all general interests, the Monarchy, revives authority, liberty, prosperity and honour. I associate myself with the work for the restoration of the Monarchy. I pledge myself to serve it by the means in my power.



Potential Advisors:

Maurice Pujo - Journalist, - Master of Propaganda?

Maxime Real del Sarte - Leader of Camelos du Roi, Nationalist French youth organization. - Possible "Indoctrination Expert" increasing manpower gains.

General and Advisor - Phillippe Petain

Advisor for "Army Leader" - Gives an increase to offense and morale.

Charles Marraus - "Mobilizer" adds war support, increases recruitable manpower.
