Set objFso = CreateObject("Scripting.FileSystemObject")
Set Folder = objFSO.GetFolder("C:\Users\[REDACTED]\Documents\Paradox Interactive\Hearts of Iron IV\mod\redvistula\gfx\flags")

For Each File In Folder.Files
    sNewFile = File.Name
    sNewFile = Replace(sNewFile,"SAU_authoritarianism","SAU_totalitarianism")
    sNewFile = Replace(sNewFile,"ETH_authoritarianism","ETH_totalitarianism")
    sNewFile = Replace(sNewFile,"YEM_authoritarianism","YEM_totalitarianism")
    sNewFile = Replace(sNewFile,"OMA_authoritarianism","OMA_totalitarianism")
    sNewFile = Replace(sNewFile,"TIB_authoritarianism","TIB_totalitarianism")
    sNewFile = Replace(sNewFile,"NEP_authoritarianism","NEP_totalitarianism")
    if (sNewFile<>File.Name) then 
        File.Move(File.ParentFolder+"\"+sNewFile)
    end if

Next

Set objFso = CreateObject("Scripting.FileSystemObject")
Set Folder = objFSO.GetFolder("C:\Users\[REDACTED]\Documents\Paradox Interactive\Hearts of Iron IV\mod\redvistula\gfx\flags\medium")

For Each File In Folder.Files
    sNewFile = File.Name
    sNewFile = Replace(sNewFile,"SAU_authoritarianism","SAU_totalitarianism")
    sNewFile = Replace(sNewFile,"ETH_authoritarianism","ETH_totalitarianism")
    sNewFile = Replace(sNewFile,"YEM_authoritarianism","YEM_totalitarianism")
    sNewFile = Replace(sNewFile,"OMA_authoritarianism","OMA_totalitarianism")
    sNewFile = Replace(sNewFile,"TIB_authoritarianism","TIB_totalitarianism")
    sNewFile = Replace(sNewFile,"NEP_authoritarianism","NEP_totalitarianism")
    if (sNewFile<>File.Name) then 
        File.Move(File.ParentFolder+"\"+sNewFile)
    end if

Next

Set objFso = CreateObject("Scripting.FileSystemObject")
Set Folder = objFSO.GetFolder("C:\Users\[REDACTED]s\Documents\Paradox Interactive\Hearts of Iron IV\mod\redvistula\gfx\flags\small")

For Each File In Folder.Files
    sNewFile = File.Name
    sNewFile = Replace(sNewFile,"SAU_authoritarianism","SAU_totalitarianism")
    sNewFile = Replace(sNewFile,"ETH_authoritarianism","ETH_totalitarianism")
    sNewFile = Replace(sNewFile,"YEM_authoritarianism","YEM_totalitarianism")
    sNewFile = Replace(sNewFile,"OMA_authoritarianism","OMA_totalitarianism")
    sNewFile = Replace(sNewFile,"TIB_authoritarianism","TIB_totalitarianism")
    sNewFile = Replace(sNewFile,"NEP_authoritarianism","NEP_totalitarianism")
    if (sNewFile<>File.Name) then 
        File.Move(File.ParentFolder+"\"+sNewFile)
    end if

Next