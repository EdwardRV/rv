division_template = {
	name = "Kankoku chū rison-chi" 			# (Square) Infantry Division - 4 infantry regiments (3k men each), 1 field artillery regiment - abstracted into infantry,
	division_names_group = JAP_GAR_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
	}
}

units = {
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}
		location = 7125 #Seoul
		division_template = "Kankoku chū rison-chi"
		start_experience_factor = 0.3
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 11
		}
		location = 4056 #Pusan
		division_template = "Kankoku chū rison-chi"
		start_experience_factor = 0.3
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 12
		}
		location = 4056 #Pusan
		division_template = "Kankoku chū rison-chi"
		start_experience_factor = 0.3
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 13
		}
		location = 4056 #Pusan
		division_template = "Kankoku chū rison-chi"
		start_experience_factor = 0.3
	}
}