﻿division_template = {
	name = "Infanterie-Division"

	division_names_group = HAN_Inf_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
        infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
        infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
	
	support = {
	}
}
division_template = {
	name = "Volkbrigade" 		

	division_names_group = HAN_MILITIA_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		
	}
	support = {
	}
}
division_template = {
	name = "Kavallerie-Division"

	division_names_group = HAN_Cav_01

	regiments = {
		cavalry = { x = 0 y = 0 }
        cavalry = { x = 0 y = 1 }
        cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }
	}
	support = {
	}
}

units = {

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 9347
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 2
		}
		location = 6377
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 3
		}
		location = 6488
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 4
		}
		location = 3574
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 5
		}
		location = 11445
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 6
		}
		location = 482
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 7
		}
		location = 425
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 8
		}
		location = 11402
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 9
		}
		location = 9264
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 10
		}
		location = 9320
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 11
		}
		location = 425
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 12
		}
		location = 587
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 495
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 2
		}
		location = 3326
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 3
		}
		location = 9347
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 4
		}
		location = 3558
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 5
		}
		location = 589
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 6
		}
		location = 3574
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 7
		}
		location = 6488
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 8
		}
		location = 3397
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 9
		}
		location = 11445
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 10
		}
		location = 3500
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 11
		}
		location = 482
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 12
		}
		location = 9497
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 13
		}
		location = 9497
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 14
		}
		location = 3558
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 15
		}
		location = 589
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 16
		}
		location = 3574
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 17
		}
		location = 3397
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 18
		}
		location = 11445
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 19
		}
		location = 3500
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 20
		}
		location = 482
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 21
		}
		location = 9497
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 22
		}
		location = 3355
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 23
		}
		location = 6501
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 24
		}
		location = 6501
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 25
		}
		location = 6582
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 26
		}
		location = 11468
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 27
		}
		location = 11468
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 28
		}
		location = 11402
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 29
		}
		location = 526
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 30
		}
		location = 526
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 31
		}
		location = 526
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 32
		}
		location = 6218
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
	division= {	
		division_name = {
				is_name_ordered = yes
				name_order = 33
		}
		location = 6218
		division_template = "Volkbrigade"
		start_experience_factor = 0.2
	}
}

#########################
## STARTING PRODUCTION ##
#########################

instant_effect = {

	add_equipment_production = {
		equipment = {
			type = infantry_equipment_0
			creator = "HAN"
		}
		requested_factories = 1
		progress = 0.1
		efficiency = 50
	}
}

#####################