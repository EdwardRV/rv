division_template = {
	name = "Uiloun gundae"
	division_names_group = KOR_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
	}
}
units = {
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 19
		}
		location = 7204
		division_template = "Uiloun gundae"
		start_experience_factor = 0.3
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 20
		}
		location = 10115
		division_template = "Uiloun gundae"
		start_experience_factor = 0.3
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 21
		}
		location = 11977
		division_template = "Uiloun gundae"
		start_experience_factor = 0.3
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 22
		}
		location = 10085
		division_template = "Uiloun gundae"
		start_experience_factor = 0.3
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 23
		}
		location = 12085
		division_template = "Uiloun gundae"
		start_experience_factor = 0.3
	}
}