﻿division_template = {
	name = "Juntuán"				# Represents: two-division infantry corps (generally poorly-equipped, but decent experience),
	division_names_group = PRC_INF_01
									#	PRC divisions were well-trained in guerilla mountain tactics.
	regiments = {
		infantry = { x = 0 y = 0 }	# Note: Chinese divisions were brigade-sized compared other nations' armies
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
	}
	priority = 0
}
division_template = {
	name = "Renmin Jundui"				# Represents three-column local militia groups (Triangular Corps); poorly-equipped, lowest experience
	division_names_group = PRC_GAR_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
	}
	priority = 1
}

units = {
	##### Eighth Route Army (CO: Mao Zedong, Zhu De) #####
	division = {
		name = "115 Bubing Shi"
		location = 11771
		division_template = "Juntuán"
		start_experience_factor = 0.3
		start_equipment_factor = 0.5

	}
	division = {
		name = "120 Bubing Shi"
		location = 6851
		division_template = "Juntuán"
		start_experience_factor = 0.3
		start_equipment_factor = 0.8

	}
	division = {
		name = "129 Bubing Shi"
		location = 10433
		division_template = "Juntuán"
		start_experience_factor = 0.3
		start_equipment_factor = 0.8

	}
	
	### Provincial Forces ###
	division = { #  "1 Shensi Juntuán"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 12448
		division_template = "Renmin Jundui"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	division = { #  "2 Shensi Juntuán"
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 881
		division_template = "Renmin Jundui"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	division = { #  "3 Shensi Juntuán"
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 9905
		division_template = "Renmin Jundui"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	division = { #  "4 Shensi Juntuán"
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 12348
		division_template = "Renmin Jundui"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	division = { #  "5 Shensi Juntuán"
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 4655
		division_template = "Renmin Jundui"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	
	division = { #  "6 Shensi Juntuán"
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 3955
		division_template = "Renmin Jundui"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	
	division = { #  "7 Shensi Juntuán"
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 6860
		division_template = "Renmin Jundui"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	
	division = { #  "8 Shensi Juntuán"
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 9803
		division_template = "Renmin Jundui"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	
	division = { #  "9 Shensi Juntuán"
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 11781
		division_template = "Renmin Jundui"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	
	division = { #  "10 Shensi Juntuán"
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 3929
		division_template = "Renmin Jundui"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
}

##### STARTING PRODUCTION #####
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_0
			creator = "PRC"
		}
		requested_factories = 3
		progress = 0.33
		efficiency = 100
	}
}