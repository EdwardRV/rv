division_template = {
	name = "Infanterie-Division"

	division_names_group = BAY_Inf_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
        infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
        infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
	
	support = {
	}
}
division_template = {
	name = "Schutzbrigade" 		

	division_names_group = BAY_MILITIA_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		
	}
	support = {
	}
}
division_template = {
	name = "Kavallerie-Division"

	division_names_group = BAY_Cav_01

	regiments = {
		cavalry = { x = 0 y = 0 }
        cavalry = { x = 0 y = 1 }
        cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }
	}
	support = {
	}
}
division_template = {
	name = "Motorisierte Sturmabteilung"

	division_names_group = BAY_MOT_01

	regiments = {
		motorized = { x = 0 y = 0 }
        motorized = { x = 0 y = 1 }
        motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
	}
	support = {
	}
}
division_template = {
	name = "Panzer-Division"

	division_names_group = BAY_ARM_01

	regiments = {
		light_armor = { x = 0 y = 0 }
        light_armor = { x = 0 y = 1 }
        light_armor = { x = 1 y = 0 }
		light_armor = { x = 1 y = 1 }
	}
	support = {
	}
}

units = {

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 3530
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 3530
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 3530
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 6568
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 6568
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 6594
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 6594
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 9572
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 11417
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 11417
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 11417
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
		location = 13116
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}
		location = 13116
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 13116
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}
		location = 6421
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 11
		}
		location = 6421
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 12
		}
		location = 3474
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 3474
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 11404
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
		location = 11404
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}
		location = 9517
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}
		location = 692
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 13
		}
		location = 692
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 14
		}
		location = 692
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 15
		}
		location = 11544
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 11
		}
		location = 11517
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 12
		}
		location = 9570
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 13
		}
		location = 11467
		division_template = "Infanterie-Division"
		start_experience_factor = 0.4
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 15
		}
		location = 6462
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 17
		}
		location = 3283
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 18
		}
		location = 3510
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 19
		}
		location = 3510
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 20
		}
		location = 3510
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 21
		}
		location = 3510
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 22
		}
		location = 3283
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 22
		}
		location = 3283
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}

	division= {	
		division_name = {
			is_name_ordered = yes
			name_order = 22
		}
		location = 3283
		division_template = "Schutzbrigade"
		start_experience_factor = 0.3
	}
}