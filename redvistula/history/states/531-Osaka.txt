
state={
	id=531
	name="STATE_531"
	resources={
		aluminium=3.000
	}

	history={
		owner = JAP
		add_core_of = JAP
		buildings = {
			infrastructure = 8
			arms_factory = 1
			industrial_complex = 2
			dockyard = 1
			7072 = {
				naval_base = 3
			}
		}
		victory_points = {
			7072 40 
		}
	}

	provinces={
		1051 1133 4054 7072 7113 
	}
	manpower=6420300
	buildings_max_level_factor=1.000
	state_category=metropolis
}
