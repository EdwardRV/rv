state={
	id=527
	name="STATE_527"
	manpower = 3464192
	state_category = town
	resources={
		steel=18 # was: 30
		tungsten=28 # was: 52
	}

	history={
		owner = JAP
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			air_base = 5
			4052 = {
				naval_base = 1
			}
		}
		
		victory_points = {
			4052 10
		}
		victory_points = {
			11835 3
		}
		add_core_of = KOR
	}

	provinces={
		906 912 978 3912 4052 6963 9790 9795 9918 11775 11828 11835 11896
	}
}
