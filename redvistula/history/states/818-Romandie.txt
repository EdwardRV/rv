
state={
	id=818
	name="STATE_818"
	provinces={
		3612 6666 6683 9622 11590 13124 
	}
	history={
		owner = SWI
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 2
		}
		add_core_of = SWI
	}
	manpower=926200
	buildings_max_level_factor=1.000
	state_category=large_city
}
