
state={
	id=50
	name="STATE_50"
	resources={
		aluminium=6.000
	}

	history={
		owner = GER
		victory_points = {
			11499 3 
		}
		buildings = {
			infrastructure = 8

		}
		add_core_of = GER

	}

	provinces={
		519 694 3690 6555 6934 9545 9655 11499 13126 
	}
	manpower=2231442
	buildings_max_level_factor=1.000
	state_category=large_city
}
