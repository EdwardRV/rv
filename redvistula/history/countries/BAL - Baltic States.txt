﻿capital = 12

oob = "BAL_1936"
if = {
	limit = { has_dlc = "Man the Guns" }
		set_naval_oob = "BAL_1936_naval_mtg"
	else = {
		set_naval_oob = "BAL_1936_naval_legacy"
	}
}

add_ideas = {
	limited_conscription
	soviet_levy
	three_cultures
	bal_planned_economy
}

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	early_fighter = 1
	tech_support = 1
	gw_artillery = 1
}
if = {
	limit = { not = { has_dlc = "Man the Guns" } }
	set_technology = {
		early_submarine = 1
	}
}
if = {
	limit = { has_dlc = "Man the Guns" }
	set_technology = {
		submarine_mine_laying = 1
		early_ship_hull_submarine = 1
	}
}

set_research_slots = 3

set_convoys = 10

set_politics = {
	ruling_party = marxism
	last_election = "1931.10.3"
	election_frequency = 108
	elections_allowed = no
}
set_popularities = {
	social_democracy = 18
	radical_nationalism = 3
	marxism = 72
	authoritarianism = 7
}

create_country_leader = {
	name = "Karlis Ulmanis"
	desc = "POLITICS_KARLIS_ULMANIS_DESC"
	picture = "GFX_Portrait_latvia_karlis_ulmanis"
	expire = "1965.1.1"
	ideology = conservatism_ideology
	traits = {
		#
	}
}

create_country_leader = {
	name = "Gustavs Celmiņš"
	desc = ""
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_land_3.dds"
	expire = "1965.1.1"
	ideology = radical_nationalism_ideology
	traits = {
		#
	}
}
create_country_leader = {
	name = "Fricis Menders"
	desc = ""
	picture = "GFX_portrait_lat_fricis_menders"
	expire = "1965.1.1"
	ideology = social_democracy_ideology
	traits = {
		#
	}
}
create_country_leader = {
	name = "Vilis Lacis"
	desc = "POLITICS_VILIS_LACIS_DESC"
	picture = "gfx/leaders/BAL/Portrait_BAL_Vilis_Lacis.dds"
	expire = "1965.1.1"
	ideology = marxism_ideology
	traits = {
		#
	}
}

create_corps_commander = {
	name = "Berkis Krisjanis"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_1.dds"
	traits = { ranger }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 1
}

### VARIANTS ###
# 1936 Start #
if = {
	limit = { not = { has_dlc = "Man the Guns" } }
	### Ship Variants ###
}
if = {
	limit = { has_dlc = "Man the Guns" }
	# Submarines #
	create_equipment_variant = {
		name = "Ronis Class"								
		type = ship_hull_submarine_1
		name_group = LAT_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = empty
		}
	}
}