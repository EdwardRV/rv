﻿capital = 64

oob = "GER_1936"

set_fuel_ratio = 0.8

# Starting tech
set_technology = {
	infantry_weapons = 1
	tech_recon = 1
	tech_support = 1		
	tech_engineers = 1
	tech_mountaineers = 1
	motorised_infantry = 1
	gw_artillery = 1
	interwar_antiair = 1
	gwtank = 1
	basic_heavy_tank = 1
	early_fighter = 1
	
	trade_interdiction = 1
	formation_flying = 1
	synth_oil_experiments = 1
	fuel_silos = 1
	fuel_refining = 1
}

if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	set_technology = {
		early_submarine = 1
		early_destroyer = 1
		early_light_cruiser = 1
		early_heavy_cruiser = 1
		early_battleship = 1
		early_battlecruiser = 1
		transport = 1
	}
	set_naval_oob = "GER_1936_naval"
}
if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	set_technology = {
		basic_naval_mines = 1
		submarine_mine_laying = 1
		early_ship_hull_light = 1
		early_ship_hull_cruiser = 1
		early_ship_hull_heavy = 1
		panzerschiffe = 1
		pre_dreadnoughts = 1
		early_ship_hull_submarine = 1
		basic_ship_hull_submarine = 1
		mtg_transport = 1
		basic_torpedo = 1
		improved_ship_torpedo_launcher = 1
		basic_depth_charges = 1
	}
	set_naval_oob = "GER_1936_naval_mtg"
}


set_research_slots = 4
set_convoys = 200
set_stability = 0.43
set_war_support = 0.35

#Trade

set_politics = {
	ruling_party = authoritarianism
	last_election = "1933.3.5"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	totalitarianism = 14
   	authoritarianism = 41
    radical_nationalism = 3
    conservatism = 14
    liberalism = 10
    social_democracy = 8
    socialism = 3
    marxism = 5
    industrialism = 2
}
add_ideas = {
	GER_economic_crisis
	GER_creaking_government
	GER_throttled_reichswehr
	GER_wilhelm_iii
	limited_exports
	disarmed_nation
	civilian_economy
	hjalmar_schacht
	herbert_von_dirksen
	kurt_von_schleicher
}

create_country_leader = {
	name = "Josef Terboven"
	desc = "POLITICS_JOSEF_TERBOVEN_DESC"
	picture = "Portrait_GER_Josef_Terboven.dds"
	expire = "1965.1.1"
	ideology = authoritarianism_ideology
	traits = {
	}
}

create_country_leader = {
	name = "Martin Bormann"
	desc = "POLITICS_MARTIN_BORMANN_DESC"
	picture = "Portrait_GER_Martin_Bormann.dds"
	expire = "1965.1.1"
	ideology = radical_nationalism_ideology
	traits = {
	}
}

create_country_leader = {
	name = "Konrad Adenauer"
	desc = "POLITICS_KONRAD_ADENAUER_DESC"
	picture = "Portrait_GER_Konrad_Adenauer.dds"
	expire = "1965.1.1"
	ideology = conservatism_ideology
	traits = {
	}
}

create_country_leader = {
	name = "Waldemar Koch"
	desc = "POLITICS_WALDEMAR_KOCH_DESC"
	picture = "Portrait_GER_Waldemar_Koch.dds"
	expire = "1965.1.1"
	ideology = liberalism_ideology
	traits = {
	}
}

create_country_leader = {
	name = "Otto Wels"
	desc = "POLITICS_OTTO_WELS_DESC"
	picture = "Portrait_GER_Otto_Wels.dds"
	expire = "1965.1.1"
	ideology = social_democracy_ideology
	traits = {
	}
}

create_country_leader = {
	name = "Ernst Thälmann"
	desc = "POLITICS_ERNST_THALMANN_DESC"
	picture = "Portrait_GER_Ernst_Thalmann.dds"
	expire = "1965.1.1"
	ideology = marxism_ideology
	traits = {
	
	}
}

create_country_leader = {
	name = "Gregor Strasser"
	desc = "POLITICS_GREGOR_STRASSER_DESC"
	picture = "Portrait_GER_Gregor_Strasser.dds"
	expire = "1965.1.1"
	ideology = industrialism_ideology
	traits = {
	
	}
}

add_namespace = {
	name = "ger_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Gerd von Rundstedt"
	picture = "Portrait_GER_Gerd_von_Rundstedt.dds"
	traits = { defensive_doctrine offensive_doctrine old_guard }
	skill = 3
	attack_skill = 3
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
	id = 1
}

create_field_marshal = {
	name = "Günther von Kluge"
	picture = "Portrait_GER_Gunther_von_Kluge.dds"
	traits = {  offensive_doctrine armor_officer }
	skill = 3
	id = 2
	attack_skill = 3
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}

create_field_marshal = {
	name = "Kurt von Schleicher"
	picture = "Portrait_GER_Kurt_von_Schleicher.dds"
	traits = {  old_guard fortress_buster }
	skill = 4
	id = 351
	attack_skill = 4
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 3
}

create_field_marshal = {
	name = "Werner von Blomberg"
	picture = "Portrait_GER_Werner_von_Blomberg.dds"
	traits = {  defensive_doctrine politically_connected }
	skill = 4
	id = 154
	attack_skill = 1
	defense_skill = 4
	planning_skill = 5
	logistics_skill = 3
}

create_corps_commander = {
	name = "Friedrich Paulus"
	picture = "Portrait_GER_Friedrich_Paulus.dds"
	traits = { trait_cautious armor_officer }
	skill = 1
	id = 3
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Erich von Manstein"
	picture = "Portrait_GER_Erich_von_Manstein.dds"
	traits = { trickster armor_officer brilliant_strategist trait_engineer }
	skill = 4
	id = 5
	attack_skill = 2
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 4
}

create_corps_commander = {
	name = "Heinz Guderian"
	picture = "Portrait_GER_Heinz_Guderian.dds"
	traits = { trickster brilliant_strategist panzer_leader armor_officer career_officer }
	skill = 4
	id = 6
	attack_skill = 2
	defense_skill = 4
	planning_skill = 4
	logistics_skill = 3
}

create_corps_commander = {
	name = "Wilhelm List"
	picture = "Portrait_GER_Wilhelm_List.dds"
	traits = { career_officer inflexible_strategist }
	skill = 2
	id = 7
	attack_skill = 2
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 2
}

create_corps_commander = {
	name = "Ewald von Kleist"
	picture = "Portrait_GER_Ewald_von_Kleist.dds"
	traits = { armor_officer trait_cautious }
	skill = 2
	id = 8
	attack_skill = 2
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Fedor von Bock"
	picture = "Portrait_GER_Fedor_von_Bock.dds"
	traits = { harsh_leader trait_cautious }
	skill = 4
	id = 9
	attack_skill = 4
	defense_skill = 3
	planning_skill = 4
	logistics_skill = 2
}

create_field_marshal = {
	name = "Walther Model"
	picture = "Portrait_GER_Walther_Model.dds"
	traits = { defensive_doctrine brilliant_strategist trait_cautious politically_connected }
	skill = 3
	id = 10
	attack_skill = 2
	defense_skill = 4
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Maximilian von Weichs"
	picture = "Portrait_GER_Maximilian_von_Weichs.dds"
	traits = { cavalry_officer }
	skill = 4
	id = 11
	attack_skill = 4
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 4
}

create_corps_commander = {
	name = "Wilhelm Ritter von Leeb"
	picture = "Portrait_GER_Wilhelm_Ritter_von_Leeb.dds"
	traits = { old_guard infantry_officer }
	skill = 3
	id = 13
	attack_skill = 4
	defense_skill = 3
	planning_skill = 1
	logistics_skill = 2
}

create_corps_commander = {
	name = "Kurt Student"
	picture = "Portrait_GER_Kurt_Student.dds"
	traits = { commando trait_reckless }
	skill = 4
	id = 16
	attack_skill = 4
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 3
}

create_corps_commander = {
	name = "Erwin von Witzleben"
	picture = "Portrait_GER_Erwin_von_Witzleben.dds"
	traits = { infantry_officer career_officer }
	skill = 4
	id = 17
	attack_skill = 4
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 4
}

create_corps_commander = {
	name = "Johannes Blaskowitz"
	picture = "Portrait_GER_Johannes_Blaskowitz.dds"
	traits = { infantry_officer }
	skill = 3
	id = 19
	attack_skill = 3
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
}

create_corps_commander = {
	name = "Friedrich Schulz"
	picture = "Portrait_GER_Friedrich_Schulz.dds"
	traits = { infantry_officer }
	skill = 3
	id = 21
	attack_skill = 3
	defense_skill = 3
	planning_skill = 1
	logistics_skill = 3
}

create_corps_commander = {
	name = "Georg von Küchler"
	picture = "Portrait_GER_Georg_von_Kuchler.dds"
	traits = {  }
	skill = 4
	id = 22
	attack_skill = 3
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 4
}

create_corps_commander = {
	name = "Alfred Jodl"
	picture = "Portrait_GER_Alfred_Jodl.dds"
	traits = { career_officer }
	skill = 3
	id = 23
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Werner von Fritsch"
	picture = "Portrait_GER_Werner_von_Fritsch.dds"
	traits = { brilliant_strategist media_personality }
	skill = 3
	id = 13636
	attack_skill = 4
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Hasso von Manteuffel"
	picture = "Portrait_GER_Hasso_von_Manteuffel.dds"
	traits = { armor_officer career_officer trait_cautious }
	skill = 4
	id = 25
	attack_skill = 4
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Albert Kesselring"
	picture = "Portrait_GER_Albert_Kesselring.dds"
	traits = { brilliant_strategist trait_cautious war_hero }
	skill = 4
	id = 32
	attack_skill = 2
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 4
}

create_corps_commander = {
	name = "Hans Speidel"
	picture = "Portrait_GER_Hans_Speidel.dds"
	traits = { defensive_doctrine }
	skill = 3
	attack_skill = 1
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Gotthard Heinrici"
	picture = "Portrait_GER_Gotthard_Heinrici.dds"
	traits = { trait_engineer infantry_officer trait_cautious }
	skill = 3

	id = 33
	attack_skill = 3
	defense_skill = 1
	planning_skill = 3
	logistics_skill = 3
}

create_navy_leader = {
	name = "Karl Dönitz"
	picture = "Portrait_Germany_Karl_Donitz.dds"
	traits = { navy_career_officer seawolf }
	skill = 5
	id = 26
	attack_skill = 5
	defense_skill = 3
	maneuvering_skill = 3
	coordination_skill = 5
}

create_navy_leader = {
	name = "Erich Raeder"
	picture = "Portrait_GER_Erich_Raeder.dds"
	traits = { naval_lineage battleship_adherent }
	skill = 4
	id = 27
	attack_skill = 3
	defense_skill = 4
	maneuvering_skill = 3
	coordination_skill = 3
}

create_navy_leader = {
	name = "Alfred Saalwächter"
		picture = "Portrait_Germany_Alfred_Saalwachter.dds"
	traits = { old_guard_navy bold}
	skill = 4
	id = 28
	attack_skill = 4
	defense_skill = 2
	maneuvering_skill = 3
	coordination_skill = 4
}

create_navy_leader = {
	name = "Reinhard Heydrich"
		picture = "Portrait_GER_Reinhard_Heydrich.dds"
	traits = { navy_media_personality cuts_corners}
	skill = 2
	id = 150
	attack_skill = 3
	defense_skill = 1
	maneuvering_skill = 4
	coordination_skill = 2
}

create_navy_leader = {
	name = "Hermann Boehm"
		picture = "Portrait_Germany_Hermann_Boehm.dds"
	traits = { craven superior_tactician }
	skill = 4
	id = 29
	attack_skill = 3
	defense_skill = 4
	maneuvering_skill = 4
	coordination_skill = 2
}

create_navy_leader = {
	name = "Wilhelm Marschall"
		picture = "Portrait_Germany_Wilhelm_Marschall.dds"
	traits = { old_guard_navy bold}
	skill = 4
	id = 30
	attack_skill = 4
	defense_skill = 2
	maneuvering_skill = 3
	coordination_skill = 4
}

create_navy_leader = {
	name = "Günther Lütjens"
		picture = "Portrait_Germany_Gunter_Lutjens.dds"
	traits = { gentlemanly navy_career_officer superior_tactician }
	skill = 3
	id = 31
	attack_skill = 4
	defense_skill = 1
	maneuvering_skill = 1
	coordination_skill = 4
}

if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	### Ship Variants ###
	## 1936 Start ##
	# Light Cruisers #
	create_equipment_variant = {
		name = "Königsberg Class"
		type = light_cruiser_1
		parent_version = 0
		upgrades = {
			ship_reliability_upgrade = 2
			ship_engine_upgrade = 2
			ship_gun_upgrade = 2
			ship_anti_air_upgrade = 2
		}
		obsolete = yes
	}
	# Heavy Cruisers #
	create_equipment_variant = {
		name = "Deutschland Class"
		type = heavy_cruiser_1
		parent_version = 0
		upgrades = {
			ship_reliability_upgrade = 1
			ship_engine_upgrade = 1
			ship_armor_upgrade = 1
			ship_gun_upgrade = 2
		}
	}
}
if = {
	limit = { has_dlc = "Man the Guns" }
	create_equipment_variant = {
		name = "Deutschland Class"
		type = ship_hull_cruiser_panzerschiff
		name_group = GER_CA_HISTORICAL
		parent_version = 0
		modules = {
        	fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_battery_slot = ship_heavy_battery_2
			fixed_ship_secondaries_slot = ship_secondaries_1
			fixed_ship_armor_slot = ship_armor_cruiser_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_engine_slot = cruiser_ship_engine_1
			rear_1_custom_slot = ship_torpedo_1
			mid_1_custom_slot = ship_airplane_launcher_1
    	}
	}
	create_equipment_variant = {
		name = "Type 24 Class"
		type = ship_hull_light_1
		name_group = GER_TB_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = empty
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			rear_1_custom_slot = ship_mine_layer_1
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Zerstörer 1934 Class"
		type = ship_hull_light_2
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_2
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_torpedo_1
			rear_1_custom_slot = ship_mine_layer_1
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Zerstörer 1934A Class"
		type = ship_hull_light_2
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_2
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_torpedo_1
			rear_1_custom_slot = ship_depth_charge_1
		}
	}
	create_equipment_variant = {
		name = "Type II Class"
		type = ship_hull_submarine_1
		name_group = GER_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Type VII Class"
		type = ship_hull_submarine_2
		name_group = GER_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_2
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = empty
		}
	}
	create_equipment_variant = {
		name = "Königsberg Class"
		type = ship_hull_cruiser_1
		name_group = GER_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_medium_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_1
			mid_1_custom_slot = ship_torpedo_1
			mid_2_custom_slot = ship_light_medium_battery_1
			rear_1_custom_slot = ship_mine_layer_1
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Emden Class"
		type = ship_hull_cruiser_1
		name_group = GER_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_medium_battery_1
			fixed_ship_anti_air_slot = empty
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_1
			mid_1_custom_slot = ship_light_medium_battery_1
			mid_2_custom_slot = ship_torpedo_1
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Bremse Class"
		type = ship_hull_cruiser_1
		name_group = GER_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			mid_1_custom_slot = ship_anti_air_1
			mid_2_custom_slot = ship_mine_layer_1
			rear_1_custom_slot = ship_mine_layer_1
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Leipzig Class"
		type = ship_hull_cruiser_2
		name_group = GER_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_medium_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_1
			front_1_custom_slot = ship_anti_air_1
			mid_1_custom_slot = ship_torpedo_1
			mid_2_custom_slot = ship_light_medium_battery_1
			rear_1_custom_slot = ship_airplane_launcher_1
		}
	}
	create_equipment_variant = {
		name = "Admiral Hipper Class"
		type = ship_hull_cruiser_2
		name_group = GER_CA_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_medium_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_2
			front_1_custom_slot = ship_anti_air_1
			mid_1_custom_slot = ship_torpedo_1
			mid_2_custom_slot = ship_airplane_launcher_1
			rear_1_custom_slot = ship_medium_battery_2
		}
	}
	create_equipment_variant = {
		name = "Scharnhorst Class"
		type = ship_hull_heavy_2
		name_group = GER_BB_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_heavy_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = heavy_ship_engine_2
			fixed_ship_secondaries_slot = ship_secondaries_1
			fixed_ship_armor_slot = ship_armor_bb_1
			front_1_custom_slot = ship_anti_air_1
			mid_1_custom_slot = empty
			mid_2_custom_slot = ship_secondaries_1
			rear_1_custom_slot = ship_heavy_battery_2
		}
	}
	create_equipment_variant = {
		name = "Deutschland Class"
		type = ship_hull_pre_dreadnought
		name_group = GER_BB_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_heavy_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = heavy_ship_engine_1
			fixed_ship_secondaries_slot = ship_secondaries_1
			fixed_ship_armor_slot = ship_armor_bb_1
			front_1_custom_slot = ship_anti_air_1
			mid_1_custom_slot = empty
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
}

create_equipment_variant = {
	name = "Ju 86"
	type = tac_bomber_equipment_0
	upgrades = {
		plane_tac_bomb_upgrade = 5
		plane_range_upgrade = 5
		plane_engine_upgrade = 5
		plane_reliability_upgrade = 10
	}
	obsolete = yes
}

create_equipment_variant = {
	name = "Do 17"
	type = tac_bomber_equipment_0
	upgrades = {
		plane_tac_bomb_upgrade = 5
		plane_range_upgrade = 1
		plane_engine_upgrade = 1
		plane_reliability_upgrade = 5
	}
	obsolete = yes
}


## 1939 Start ##
1939.1.1 = {
	if = {
		limit = {
			not = { has_dlc = "Man the Guns" }
		}
		# Submarines #
		create_equipment_variant = {
			name = "Type VIIA"
			type = submarine_2
			parent_version = 0
			upgrades = {
				ship_reliability_upgrade = 1
				sub_engine_upgrade = 1
				sub_stealth_upgrade = 1
				sub_torpedo_upgrade = 1
			}
		}
		# Destroyer #
		create_equipment_variant = {
			name = "Zerstörer 1936"
			type = destroyer_2
			parent_version = 0
			upgrades = {
				ship_torpedo_upgrade = 2
				destroyer_engine_upgrade = 2
				ship_ASW_upgrade = 2
				ship_anti_air_upgrade = 2
			}
		}
	}
	if = {
		limit = {
			has_dlc = "Man the Guns"
		}
		create_equipment_variant = {
			name = "Zerstörer 1936 Class"
			type = ship_hull_light_2
			name_group = GER_DD_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_light_battery_2
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = light_ship_engine_2
				fixed_ship_torpedo_slot = ship_torpedo_1
				rear_1_custom_slot = ship_depth_charge_1
			}
		}
		create_equipment_variant = {
			name = "Bismarck Class"
			type = ship_hull_heavy_2
			name_group = GER_BB_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_heavy_battery_3
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_1
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = heavy_ship_engine_2
				fixed_ship_secondaries_slot = ship_secondaries_2
				fixed_ship_armor_slot = ship_armor_bb_2
				front_1_custom_slot = ship_anti_air_1
				mid_1_custom_slot = ship_airplane_launcher_1
				mid_2_custom_slot = ship_secondaries_2
				rear_1_custom_slot = ship_heavy_battery_3
			}
		}
		create_equipment_variant = {
			name = "Graf Zeppelin Class"
			type = ship_hull_carrier_1
			name_group = GER_CV_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_deck_slot_1 = ship_deck_space
				fixed_ship_deck_slot_2 = ship_deck_space
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = carrier_ship_engine_2
				fixed_ship_secondaries_slot = ship_secondaries_1
				front_1_custom_slot = ship_deck_space
			}
		}
	}
	create_equipment_variant = {
		name = "He 112"
		type = fighter_equipment_1
		upgrades = {
			plane_gun_upgrade = 1
			plane_range_upgrade = 1  
			plane_engine_upgrade = 0
			plane_reliability_upgrade = 2
		}
	}
	create_equipment_variant = {
		name = "Bf 109 E"
		type = fighter_equipment_1
		upgrades = {
			plane_gun_upgrade = 2
			plane_range_upgrade = 1  
			plane_engine_upgrade = 2
			plane_reliability_upgrade = 4
		}
	}
	# Heavy Cruisers #
	#create_equipment_variant = {
	#	name = "Deutschland Class"
	#	type = heavy_cruiser_1
	#	parent_version = 0
	#	upgrades = {
	#		ship_reliability_upgrade = 1
	#		ship_engine_upgrade = 1
	#		ship_armor_upgrade = 1
	#		ship_gun_upgrade = 2
	#	}
	#	obsolete = yes
	#}
}