﻿capital = 324

oob = "BHU_1936"

# Starting tech
set_technology = {
	infantry_weapons = 1
	tech_mountaineers = 1
}
set_war_support = 0.1
set_stability = 0.8
if = {
	limit = {
		has_dlc = "Together for Victory"
	}
	ENG = {
		set_autonomy = {
			target = ROOT
			autonomy_state = autonomy_rv_brit_protectorate
		}
	}
	else = {
		ENG = {
			puppet = ROOT
		}
	}
}
set_politics = {
	ruling_party = authoritarianism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	authoritarianism = 100
}

create_country_leader = {
	name = "Jigme Wangchuck"
	desc = "POLITICS_JIGME_WANGCHUCK_DESC"
	picture = "GFX_portrait_buthan_jigme_wangchuk"
	expire = "1965.1.1"
	ideology = authoritarianism_ideology
	traits = {
		#
	}
}

1939.1.1 = {
	set_politics = {
		ruling_party = authoritarianism
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		authoritarianism = 100
	}
}