﻿capital = 629

#oob = ""

# Starting tech
set_technology = {
	infantry_weapons = 1
}

set_convoys = 20
set_politics = {
	ruling_party = authoritarianism
	last_election = "1936.1.1"
	elections_allowed = no
}
set_popularities = {
	liberalism = 38
	radical_nationalism = 6
	marxism = 6
	authoritarianism = 50
}

create_country_leader = {
	name = "David Kalakaua Kawananakoa"
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_land_5.dds"
	ideology = authoritarianism_ideology
	traits = {
		#
	}
}

create_country_leader = {
	name = "David Kalakaua Kawananakoa"
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_land_5.dds"
	ideology = radical_nationalism_ideology
	traits = {
		#
	}
}

create_country_leader = {
	name = "Joseph Poindexter"
	desc = ""
	picture = "gfx/leaders/USA/Portrait_USA_Generic_land_1.dds"
	ideology = liberalism_ideology
	traits = {
		#
	}
}

create_country_leader = {
	name = "Charles Fujimoto"
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_1.dds"
	ideology = marxism_ideology
	traits = {
		#
	}
}
