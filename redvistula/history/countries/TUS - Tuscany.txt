capital = 162

set_research_slots = 2
set_stability = 0.50
set_war_support = 0.25

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1		
	tech_engineers = 1
	tech_mountaineers = 1
	motorised_infantry = 1
	gw_artillery = 1
	interwar_antiair = 1
	gwtank = 1
	basic_light_tank = 1
	#basic_heavy_tank = 1  # PLACEHOLDER
	#basic_medium_tank = 1 # PLACEHOLDER
	early_fighter = 1
	early_bomber = 1
	naval_bomber1 = 1
	CAS1 = 1
	trench_warfare = 1
	fleet_in_being = 1
	fuel_silos = 1
	fuel_refining = 1
}

set_politics = {
	ruling_party = authoritarianism
	last_election = 1935.1.1
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	social_democracy = 7
	marxism = 5
	authoritarianism = 63
	liberalism = 13
	conservatism = 2
	socialism = 10
}
create_country_leader = {
	name = "Italian Man"
	desc = ""
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
	expire = "1965.1.1"
	ideology = authoritarianism_ideology
	traits = {
	#
	}
}