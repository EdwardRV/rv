add_namespace = rvParaguay
country_event = {
	id = rvParaguay.1
	title = rvParaguay.1.t
	desc = rvParaguay.1.desc
	picture = GFX_country_event_rv_PAR_protest2
	fire_only_once = yes
	mean_time_to_happen = {
		days = 1
	}
	trigger = { TAG = PAR }
	immediate = {
		log = "Event triggered for [ROOT.GetTag]: rvParaguay.1"
	}

	option = {
		name = rvParaguay.1.a
		news_event = rvParaguay.2
		PAR = {
			set_politics = {
				elections_allowed = no
				ruling_party = marxism
			}
			add_popularity = {
				ideology = marxism
				popularity = 0.32
			}
		}
	}
}
news_event = {
	id = rvParaguay.2
	title = rvParaguay.2.t
	desc = rvParaguay.2.d
	picture = GFX_news_event_rv_PAR_protest1
	major = yes

	is_triggered_only = yes

	option = {
		name = rvParaguay.2.a
		trigger = {
			has_socialist_government = yes
		}
	}
	option = {
		name = rvParaguay.2.b
		trigger = {
			has_socialist_government = no
		}
	}
}